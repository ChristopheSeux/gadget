
import sys

from canevas.ui import Application, StyleSheet


from canevas.ui.base_widgets import VWidget, HWidget, PushButton, SpinBox, Frame, Icon, \
FWidget, TabWidget

from canevas.ui.properties import IntProperty, FloatProperty, ColorProperty, TextProperty, \
PathProperty, StringProperty, ListProperty, EnumProperty, BoolProperty

from PySide2.QtCore import QSize, Qt
from PySide2.QtGui import QColor, QIcon, QPainter, QPixmap, QImage
from PySide2.QtWidgets import QPushButton, QWidget, QBoxLayout, QProxyStyle
from PySide2.QtSvg import QSvgWidget

#from __feature__ import snake_case
#import appdirs

#print(appdirs.user_config_dir('gadget') )



app = Application()




class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        self.setGeometry(300, 300, 350, 300)

    def paintEvent(self, event):
        qp = QPainter()
        qp.begin(self)
        self.drawText(event, qp)
        qp.end()

    def drawText(self, event, qp):
        qp.setPen(QColor(168, 34, 3))


class InterfaceTab(FWidget):
    def __init__(self):
        super().__init__(title='Display', align='Top', spacing=8, margin=8,
        Background='dark')

        settings = app.context.settings

        settings.display['ui_scale'].to_widget(self, mode='expand')
        settings.display['theme'].to_widget(self, mode='expand')

        #for k, v in settings.display['color'].items():
        #    v.to_widget(self)




class WidgetsTab(FWidget):
    def __init__(self):
        super().__init__(title='Widgets', align='Top', spacing=2, margin=8,
        Background='dark')

        icon_path = '/home/christophe/Documents/Programmation/doliprane-2.1.0/gadget/resources/icons/plus.svg'

        btn = PushButton(self, text='My Button', icon=icon_path,  size=(None, 68))
        btn.setIconSize(QSize(32,32))
        btn = PushButton(self, text='My Button', icon=icon_path,  size=(None, 68))
        btn.setIconSize(QSize(32,32))

        #btn.setDisabled(True)
        #PushButton(self, text='', tag='Close')
        #frame = Example()

        '''

        icon_path = '/home/christophe/Documents/Programmation/doliprane-2.1.0/gadget/resources/icons/folder/folder_32.png'

        icon_path = '/home/christophe/Téléchargements/pen-31.svg'
        icon_path = '/home/christophe/Documents/Programmation/doliprane-2.1.0/gadget/resources/icons/folder.svg'
        #icon_path = '/home/christophe/Téléchargements/icons_check-square.svg'

        size = 64, 64

        icon = Icon()
  
   
        #default_pixmap = QPixmap(icon_path).scaled(*size)
        default_pixmap = QIcon(icon_path).pixmap(QSize(*size))

        gray_img = default_pixmap.toImage().convertToFormat(QImage.Format_Grayscale8)
        gray_pix = QPixmap.fromImage(gray_img)

        shadow = QPixmap(*size)
        shadow.fill(Qt.transparent)
        painter = QPainter(shadow)
        painter.setOpacity(0.25)
        painter.drawPixmap(-2, -2, default_pixmap)
        painter.drawPixmap(2, 2, default_pixmap)
        painter.drawPixmap(4, 4, default_pixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.fillRect(shadow.rect(), QColor(0,0,0))
        painter.end()

        overlay = QPixmap(*size)
        overlay.fill(Qt.transparent)
        painter = QPainter(overlay)
        painter.drawPixmap(0, 0, default_pixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.fillRect(overlay.rect(), QColor(255,255,255))
        painter.end()

        normal_pixmap = QPixmap(shadow)
        painter = QPainter(normal_pixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
        painter.drawPixmap(0, 0, default_pixmap)

        disabled_pixmap = QPixmap(normal_pixmap)
        painter = QPainter(disabled_pixmap)
        painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
        painter.drawPixmap(0, 0, gray_pix)
        painter.end()
        
        highlight_pixmap = QPixmap(normal_pixmap)
        painter = QPainter(highlight_pixmap)
        painter.setCompositionMode(QPainter.CompositionMode_Overlay)
        painter.setOpacity(0.25)
        painter.drawPixmap(0, 0, overlay)

        #icon.addPixmap(pix)
        icon.addPixmap(normal_pixmap, QIcon.Normal)
        icon.addPixmap(highlight_pixmap, QIcon.Active)
        icon.addPixmap(disabled_pixmap, QIcon.Disabled)
        #icon.addPixmap(default_pixmap.scaled(32, 32), QIcon.Disabled)

        

        btn.setIcon(icon)
        btn.setIconSize(QSize(*size))

        #self.layout().addWidget(frame)
        '''



class PropertiesTab(FWidget):
    def __init__(self):
        super().__init__(title='Widgets', align='Top', spacing=8, margin=8,
        Background='dark')

        self.int_prop = IntProperty(parent=self, name='Int', reset=True)
        self.float_prop = FloatProperty(parent=self, name='Float', reset=True)
        self.color_prop = ColorProperty(parent=self, name='Color')
        self.text_prop = TextProperty(parent=self, name='Text')
        self.path_prop = PathProperty(parent=self, name='Path')
        self.string_prop = StringProperty(parent=self, name='String', reset=True, clear=True, icon='search')
        self.list_prop = ListProperty(parent=self, name='List')
        self.enum_prop = EnumProperty(parent=self, name= 'Enum', items=['Test1', 'Test2', 'Test3'], mode='expand', exclusive=False)
        self.bool_prop = BoolProperty(parent=self, name='Bool')

        print('Props')


class Window(VWidget):
    def __init__(self):
        super().__init__(title='Window', align='Top', spacing=0, margin=4,
        Background='very_dark')    


        self.tab_widget = TabWidget(self)
        #self.tab_widget.setDocumentMode(True)
        #self.tab_widget.taskBar.setAutoFillBackground(True)

        self.tab_widget.addTab(WidgetsTab(), 'Widgets')
        self.tab_widget.addTab(PropertiesTab(), 'Properties')
        self.tab_widget.addTab(InterfaceTab(), 'Preferences')




window = Window()





window.show()

'''
w = VWidget()


btn = PushButton(w, 'Text')


#change_btn = PushButton(w, 'Change Border')

w.resize(250, 150)
w.move(300, 300)
w.setWindowTitle('Simple')
w.show()
'''



app.exec_()
