from PySide2.QtWidgets import QApplication as QApp
from PySide2.QtWidgets import QWidget, QPushButton, QVBoxLayout, QSlider


class QApplication(QApp) :
    ui_scale_changed = Signal(float)
    ui_scale = 1.0

    def __init__(self, argv, icon_dir=None) :

        QApp.setStyle('Fusion')

        super().__init__(argv)


class UIStyle:
    def __init__(self, widget):    
        self._widget = widget

        self.
    
    def widget(self):
        return self._widget

class PushButton(QPushButton):
    def __init__(self, text):
        super().__init__(text)

        print(self.ui)

        self.ui = UIStyle(self)

        stylesheet = '''
        QPushButton{
            color : rgba(200, 200, 200, 255);
            outline : none;
            padding : 0px;
            margin : 0px;
            background : rgba(10, 10, 10, 255);
            border-style: solid;
            border-width: 1 px;
            }'''

        self.setStyleSheet(stylesheet)

app = QApplication([])
window = QWidget()
layout = QVBoxLayout()
layout.addWidget(PushButton('Top'))
layout.addWidget(PushButton('Bottom'))
layout.addWidget(QSlider())

window.setLayout(layout)




window.show()
app.exec_()