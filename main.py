
import sys
import argparse
from pathlib import Path


if __name__ == '__main__':
    if len(sys.argv)>1 and sys.argv[1].endswith('.py') :
        sys.argv = sys.argv[1:]
        exec( open(sys.argv[0]).read() )

    else :
        parser = argparse.ArgumentParser(description='Process some integers.',
        formatter_class= argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('--studio')
        parser.add_argument('--project')
        parser.add_argument('--ignore-prefs', action='store_true')
        args = parser.parse_args()

        from gadget.ui import Application

        if args.studio is None:
            args.studio = Path(__file__).parent/'demo_studio_conf.yml'

        if args.project is None:
            args.project = 'TV'

        app = Application(**vars(args))
        app.show()
