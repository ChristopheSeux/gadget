
from pathlib import Path

import gadget

from gadget.base_objects.collection import Collection

from gadget.base_objects.properties import BoolProperty, StringProperty, \
FloatProperty, IntProperty, EnumProperty

from gadget.widgets.custom_widgets import ContextWidget


class Arguments(Collection) :
    def __init__(self, command, datas=[]):
        super().__init__()

        self.command = command

        self.set(datas)
    
    def set(self, datas) :
        self.clear()

        actions = [self.add(data, emit_signal=False) for data in datas ]
        #self.project.signals.actions_updated.emit(actions)

    def add(self, data, emit_signal=True) :

        name = data.get('name', '')
        default = data.get('default')
        arg_type = list if data.get('items') else type(data.get('default') )

        if data.get('context') :
            argument = ContextArgument(self, data)
        elif arg_type is bool:
            argument = BoolArgument(self, data)
        elif arg_type is str:
            argument = StringArgument(self, data)
        elif arg_type is int:
            argument = IntArgument(self, data)
        elif arg_type is float:
            argument = FloatArgument(self, data)
        elif arg_type in (tuple, list):
            argument = EnumArgument(self, data)

        self[argument.name] = argument

        #if emit_signal :
        #    self.project.signals.actions_updated.emit([action])

        return argument
    
    def to_dict(self):
        return { a.name : a.value for a in self }


class Argument:
    def __init__(self,arguments,  data):
        #super().__init__()
        self.collection = arguments
        self.arguments = arguments

        self.name = data.get('name')
        self.default = data.get('default')
        self.arg = data.get('arg')
        self.context = data.get('context')
        self.hide = data.get('hide', False)
        
        if self.arg is None and self.name :
            self.arg = self.name.lower().replace('_', '-').replace(' ', '-')
        elif self.name is None and self.arg :
            self.name = self.arg.strip('-').replace('-', ' ').replace('_', ' ').title()

        self.norm_name = self.name.replace(' ', '_').lower()

    def to_widget(self, parent=None):
        return self.property.to_widget(parent=parent, name=self.name)

    @property
    def value(self) :
        return self.property.value


class ContextArgument(Argument):
    def __init__(self,arguments,  data):
        super().__init__(arguments, data)

    def to_widget(self, parent=None):
        return ContextWidget(self, parent=parent)

    def eval(self) :
        value = getattr(gadget.context, self.context)
        return self.to_dict(value)

    def to_dict(self, value) :
        if isinstance(value, str) :
            return value
        elif isinstance(value, (tuple, list) ) :
            return [ self.to_dict(v) for v in value]
        elif isinstance(value, Path) :
            return value.as_posix()
        else :
            return value.to_dict()

    @property
    def value(self) :
        return self.eval()


class BoolArgument(Argument):
    def __init__(self, arguments, data):
        super().__init__(arguments, data)

        self.property = BoolProperty(bool(self.default), name=self.name)


class StringArgument(Argument):
    def __init__(self, arguments, data):
        super().__init__(arguments, data)
    
        self.property = StringProperty(self.default, name=self.name)


class FloatArgument(Argument):
    def __init__(self,arguments,  data):
        super().__init__(arguments, data)

        decimals = max(3, len(str(self.default or 0).split('.')[1]))

        self.property = FloatProperty(self.default, name=self.name, min=None, max=None, decimals=decimals)


class IntArgument(Argument):
    def __init__(self,arguments,  data):
        super().__init__(arguments, data)

        self.property = IntProperty(self.default, name=self.name, min=None, max=None)


class EnumArgument(Argument):
    def __init__(self, arguments,  data):
        super().__init__(arguments, data)

        self._items = data.get('items', [])

        self.property = EnumProperty(self.default, name=self.name)

        if isinstance(self._items, (list, tuple)):
            self.property.set_items(self._items)

    def to_widget(self, parent=None) :
        if isinstance(self._items, str):
            values = getattr(gadget.context, self._items)

            if isinstance(values, Collection):
                items = values.keys()
            else :
                items = [i.name for i in values]

            self.property.set_items(items)

        super().to_widget(parent)