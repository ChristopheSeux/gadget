
import yaml
import re
from pathlib import Path

from gadget.base_objects.conf import Conf
from gadget.base_objects.collection import Collection
from gadget.base_objects.properties import BoolProperty, TextProperty,\
StringProperty, IntProperty, FloatProperty, EnumProperty
from gadget.base_objects.argument import Arguments

from gadget.widgets.custom_widgets import ActionMenu, ActionButton,\
ActionPanel, ActionDialog, CommandWidget
from gadget.widgets.base_widgets import PushButton, ListLabel, FWidget, Frame

##from gadget.constants import APP
import subprocess
import json
from PySide2.QtCore import QProcess, QIODevice, QThread
import gadget

'''
class Argument:
    def __init__(self, conf):
        #super().__init__()

        self.name = conf.get('name')
        self.default = conf.get('default')
        self.arg = conf.get('arg')
        self.type = type(self.default)
        self.data_path = conf.get('data_path', '')
        
        if self.arg is None and self.name :
            self.arg = self.name.lower().replace('_', '-').replace(' ', '-')
        elif self.name is None and self.arg :
            self.name = self.arg.strip('-').replace('-', ' ').replace('_', ' ').title()

        self.norm_name = self.name.replace(' ', '_').lower()

        self._items = conf.get('items', [])

        if self._items :
            self.type = list

    def to_dict(self, value) :
        if isinstance(value, str) :
            return value
        elif isinstance(value, (tuple, list) ) :
            return [ self.to_dict(v) for v in value]
        else :
            return value.to_dict()

    def eval(self) :
        value = eval(self.data_path)
        return self.to_dict(value)

    def to_widget(self, parent=None):
        if self.data_path:
            return DataPathArgument(self, parent)
        elif self.type is bool:
            return BoolProperty(self.default, parent=parent, name=self.name)
        elif self.type is str:
            return StringProperty(self.default, parent=parent, name=self.name)
        elif sfrom gadget.base_objectself.type is int:
            return IntProperty(self.default, parent=parent, name=self.name)
        elif self.type is float:
            return FloatProperty(self.default, parent=parent, name=self.name)
        elif self.type in (tuple, list):
            return EnumProperty(self.default, parent=parent, name=self.name,
            items=self.items)
        

    @property
    def items(self) :
        if isinstance(self._items, str):
            return [i.name for i in eval(self._items) ]
        else :
            return self._items


class BoolArgument(Argument):
    def __init__(self, conf):
        super().__init__()

    def to_widget(self, parent=None):
        return BoolProperty(self.default, parent=parent, name=self.name)

class StringArgument(Argument):
    def __init__(self, conf):
        super().__init__()

    def to_widget(self, parent=None):
        return BoolProperty(self.default, parent=parent, name=self.name)

class FloatArgument(Argument):
    def __init__(self, conf):
        super().__init__()

    def to_widget(self, parent=None):
        return BoolProperty(self.default, parent=parent, name=self.name)
'''
'''
class AppCommand:
    def __init__(self, commands):
        self.collection = commands
        self.commands = commands
        self.project = commands.project

        self.arguments = Collection()

    def to_widget(self, parent=None):
        pass
    
    def run(self) :
       for c in cmds :
            #print(f'Run Action {self.name} with this cmd : {c}')
            gadget.app.disk_worker.add_command(c)


class AddCommentCommand(AppCommand):
    def __init__(self):
        self.name = 'Add Comment'

        arguments = (
            Comment : {'default' : '', }


        )

        self.arguments.set()

        self.arguments['Comment'] = TextArgument()
        self.arguments['Tasks'] = ContextArgument()
        self.arguments['Add Preview'] = BoolArgument()
        self.arguments['Status'] = ContextArgument()
'''


class Commands(Collection):
    def __init__(self, parent, datas=[]):
        super().__init__()

        self.parent = parent
        self.set(datas)

    def set(self, datas) :
        self.clear()

        commands = [self.add(data, emit_signal=False) for data in datas ]

        self.parent.signals.commands_updated.emit(commands)

    def add(self, data, emit_signal=True) :
        command = Command(self, data)
        self[command.name] = command

        if emit_signal :
            self.parent.signals.commands_updated.emit([command])

        return command

    def run(self, files=[], **kargs) :
        for c in self :
            c.run(files=files, **kargs)



class Command(Conf):
    def __init__(self, commands, conf):
        super().__init__(conf)

        self.collection = commands
        self.commands = commands
        self.parent = commands.parent

        self.name = self.conf.get('name')
        self.path = self.conf.get('path')
        self.on_each = self.conf.get('on_each')
        self.focus = self.conf.get('focus', True)
        self.background = self.conf.get('background', False)

        soft = self.conf.get('soft','Python')
        self.soft = self.parent.softs[soft]
        self.soft_version = conf.get('soft_version', None)

        self.arguments = Arguments(self, self.conf.get('arguments', []) )
        #self.arguments = Collection.from_attr('name', [Argument(a) for a in args])

    def to_widget(self, parent=None):
        return CommandWidget(self, parent)

    def norm_arg(self, arg_name) :
        arg_name = re.sub('[-]+',' ', arg_name)
        arg_name= arg_name.strip(' ')

        return '--' + arg_name.replace(' ', '-').lower()


    def norm_value(self, value):

        if isinstance(value, (tuple, list)) :
            values = []
            for v in value :
                if not isinstance(v, str):
                    v = json.dumps(v)
                values.append(v)

            return values

        if not isinstance(value, str):
            value = json.dumps(value)
        return value

    '''
    def eval_arg(self, data_path) :
        value = eval(data_path)

        if isinstance(value, (tuple, list) ) :
            value = [ json.dumps(v.to_dict() ) for v in value]
        elif not isinstance(value, str):
            value = json.dumps( value.to_dict() )

        return value
        '''

    def get_cmds(self, files=[], **kargs) :

        norm_args = {}
        for argument in self.arguments :
            norm_args[self.norm_arg(argument.arg)] = self.norm_value(argument.value)
        '''
        for arg in self.arguments :
            arg_name = arg.arg or self.norm_arg(arg.name)

            if arg.norm_name in kargs :
                kargs[arg_name] = kargs[arg.norm_name]
                del kargs[arg.norm_name]
            elif arg_name in kargs :
                kargs[arg_name] = kargs[arg_name] # Replace Value a string for the cmdline
            elif arg.data_path :
                kargs[arg_name] = self.eval_arg(arg.data_path)
            else :
                kargs[arg_name] =  arg.default
            '''

        
        for k, v in kargs.items() :
             norm_args[self.norm_arg(k)] = self.norm_value(v)

        cmds= []

        if not files and self.on_each :
            files = getattr(gadget.context, self.on_each)

        if files :
            for f in files :
                cmds.append( self.soft.active_version.get_cmd(file=f,
                python=str(self.path), args=norm_args, focus=self.focus, background=self.background) )
        else :
            cmds = [ self.soft.active_version.get_cmd(python=str(self.path),
            args=norm_args, focus=self.focus, background=self.background) ]

        return cmds


    def run(self, files=[], **kargs) :
        cmds = self.get_cmds(files=files, **kargs)

        for c in cmds :
            #print(f'Run Action {self.name} with this cmd : {c}')
            gadget.app.disk_worker.add_cmd(c)
        #self.action_queue = ActionQueue(cmds)
        #self.action_queue.start()
        #subprocess.Popen(cmd)
        #return self.action_queue


    def to_dict(self) :
        pass


class Actions(Collection):
    def __init__(self, project, datas=[]):
        super().__init__()

        self.project = project
        #self.conf = conf

        self.set(datas)

    def set(self, datas) :
        self.clear()

        actions = [self.add(data, emit_signal=False) for data in datas ]
        self.project.signals.actions_updated.emit(actions)

    def add(self, data, emit_signal=True) :
        #print(self, data)
        action = Action(self, data)
        self[action.name] = action

        if emit_signal :
            self.project.signals.actions_updated.emit([action])

        return action


class Action:
    def __init__(self, actions, conf):

        #print(conf)

        self.collection = actions
        self.actions = actions
        self.project = actions.project
        self.conf = conf

        self.icon = conf.get('icon')
        self.name = conf.get('name', '')
        self.label = conf.get('label')
        if self.label is None:
            self.label = self.name
        self.icon = conf.get('icon')
        self.widget_name = conf.get('widget')
        #self.locations = conf.get('locations', [])
        #self.locations = [Path(l).as_posix() for l in self.locations] # Because of conf class
        self.enabled = conf.get('enabled')

        self.actions = Actions(self.project)
        self.actions.set(conf.get('actions', []))
        #print("Action", self.widget, self.name, self.actions)
        #print( conf.get('actions', []) )
        self.commands = Commands(self.project) #conf.get('commands', [])
        for cmd_name in conf.get('commands', []) :
            #print(cmd_name)
            #print(self.project.commands.keys())
            if cmd_name in self.project.commands.keys() :
               self.commands[cmd_name] = self.project.commands[cmd_name]
        
        #self.has_arguments
        #self.commands.set(conf.get('commands', []))
        #self.panels = conf.get('panels', [])
        #self.menus = conf.get('menus', [])

    def to_widget(self, parent=None):
        if self.widget_name == 'Menu':
            widget = PushButton(parent, text=self.label, icon=self.icon, tag='Menu')
            widget.setMenu( ActionMenu(self, widget) )
            return widget

        elif self.widget_name == 'Button':
            return ActionButton(self, parent=parent)

        elif self.widget_name == 'Panel':
            return ActionPanel(self, parent=parent)

        elif self.widget_name == 'Dialog':
            widget = PushButton(parent, text=self.label, icon=self.icon)
            widget.clicked.connect( lambda : ActionDialog(self, widget).show() )
            return widget

        print('Widget', self.widget, 'not implemented')
        return # A Command widget?


class Interfaces(Collection):
    def __init__(self, project, datas=[]):
        super().__init__()

        self.project = project
        #self.conf = conf

        self.set(datas)

    def set(self, datas) :
        self.clear()

        interfaces = [self.add(data, emit_signal=False) for data in datas ]
        self.project.signals.interfaces_updated.emit(interfaces)

    def add(self, data, emit_signal=True) :
        #print(self, data)
        interface = Interface(self, data)
        self[''] = interface

        if emit_signal :
            self.project.signals.interfaces_updated.emit([interface])

        return interface


class ActionWidget:
    def __init__(self, interface, conf):    

        self.interface = interface
        self.conf = conf

        self.action = self.interface.project.actions.get(conf.get('action'))
        self.widget = conf.get('widget')
    
    def to_widget(self, parent=None):
        if self.action :
            return self.action.to_widget(parent)
        elif self.widget == 'Separator' :
            return Frame(parent, tag='small')


class Interface:
    def __init__(self, interfaces, conf):

        self.collection = interfaces
        self.interfaces = interfaces
        self.project = interfaces.project
        self.conf = conf

        self.locations = conf.get('locations', [])
        self.locations = [Path(l).as_posix() for l in self.locations] # Because of conf class
        self.widgets = [ActionWidget(self, w) for w in conf.get('widgets', [])] 

        #print(self)

    def to_widget(self, parent=None):
        for w in self.widgets :
            #print('ActionWidget', w)
            w.to_widget(parent)



'''
class ActionButtonDialog(PushButton):
    def __init__(self, action, parent=None):
        super().__init__(parent, text=action.label, icon=action.icon)

        self.action = action

        self.clicked.connect(  )
        '''