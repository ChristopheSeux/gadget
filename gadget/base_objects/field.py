

class Field:
    def __init__(self, value):
        self.value = value

class Bool(Field):
    def __init__(self, value):
        super().__init__(value)

class Path(Field):
    def __init__(self, value):
        super().__init__(value)

class Color(Field):
    def __init__(self, value):
        super().__init__(value)

class String(Field):
    def __init__(self, value):
        super().__init__(value)