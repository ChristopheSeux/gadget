
from pathlib import Path
import yaml

MODULE_DIR = Path(__file__).parent
GADGET_DIR = MODULE_DIR.parent
UI_DIR = MODULE_DIR/'ui'

RESOURCES_DIR = MODULE_DIR/'resources'
ICONS_DIR = RESOURCES_DIR/'icons'
THEMES_DIR = RESOURCES_DIR/'themes'
#STYLESHEET = (UI_DIR/'stylesheet.css').read_text()

#CONFIG = yaml.safe_load( (MODULE_DIR/'config.yml').read_text() )

