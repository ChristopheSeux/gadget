
from os import environ as env
from pathlib import Path
import gazu

from gadget.constants import *



class Kitsu:
    def connect(self, url=None, login=None, password=None):

        self.project = None
        self.sequences = []
        self.tasks = []

        url = url or env['KITSU_URL']
        if not url.endswith('/api'):
            url += '/api'

        login = login or env['KITSU_LOGIN']
        password = password or env['KITSU_PASSWORD']

        print(f'Setting Host for kitsu {url}')
        gazu.client.set_host(url)
        try:
            gazu.client.host_is_up()
        except Exception as e:
            print('Host down \n', e)
            return

        try:
            print(f'Log in to kitsu as {login}')
            res = gazu.log_in(login, password)
            print(f'Sucessfully login to Kitsu as {res["user"]["full_name"]}')
            return res['user']['id']
        except Exception as e:
            print('\nFail to log', e)

    def new_shot(self, name, sequence, nb_frames=None, frame_in=None,
    frame_out=None, data={} ):
        project = project or self.project
        return gazu.asset.new_shot(
            project = project,
            sequence = sequence,
            name = name,
            data = data )

    def new_asset(self, name, asset_type, description='', project=None,
    episode=None, data={}):

        project = project or self.project

        return gazu.asset.new_asset(
            project = project,
            asset_type= asset_type,
            name = name,
            description = description,
            extra_data = data )

    def new_task(self, entity, task_type, task_status=None, assigner=None,
    assignees=None):

        return gazu.task.new_task(
            entity=entity,
            task_type=task_type,
            task_status=task_status,
            assigner=assigner,
            assignees=assignees )

    def add_preview(self, task, comment, preview):
        return gazu.task.add_preview(
            task=task,
            comment=comment,
            preview_file_path=preview )

    def add_comment(self, task, task_status=None, comment='', person=None):
        comment = gazu.task.add_comment(
            task=task,
            task_status=task_status,
            comment=comment,
            person=person )
        
        if preview:
            preview = self.add_preview(
                task=task,
                comment=comment,
                preview=preview )

        return comment

    def get_project(self, project_name):
        project_name = project_name or env['KITSU_PROJECT']
        #self.project = gazu.project.get_project_by_name(project_name)
        self.project = gazu.client.fetch_first('projects', {'name': project_name})
        return self.project

    def get_persons(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('persons', {'project_id': project['id']} )

    def get_task_status(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('task-status', {'project_id': project['id']} )

    def get_task_types(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('task-types', {'project_id': project['id']} )

    def get_entity_types(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('entity-types', {'project_id': project['id']} )

    def get_sequences(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('sequences', {'project_id': project['id']} )
        #return self.sequences

    #def get_tasks(self, project=None):
    #    project = project or self.project
    #    return gazu.client.fetch_all('tasks', { 'project_id': project['id']} )

    def get_comments(self, project=None):
        project = project or self.project
        return gazu.client.fetch_all('comments', {'project_id': project['id']} )

    def get_assets(self, project=None):
        project = project or self.project
        assets = gazu.client.fetch_all('assets/with-tasks', {'project_id': project['id']} )
        #for a in assets: # get casting
        #    a['project_id'] = project['id']
        #    a['casting'] = gazu.casting.get_asset_casting(a)

            #for task in a['tasks']:
            #    task['comments'] = [ gazu.task.get_last_comment_for_task(task) ]
            #print(a)
        return assets

    def get_shots(self, project=None):
        project = project or self.project
        shots = gazu.client.fetch_all('shots/with-tasks', {'project_id': project['id']} )
        #casting = {s['id']: gazu.casting.get_sequence_casting(s) for s in self.sequences}
        #for s in shots: # get casting
        #    s['casting'] = casting[s['parent_id']].get(s['id'], [] )

            #for task in s['tasks']:
            #    task['comments'] = [ gazu.task.get_last_comment_for_task(task) ]

        return shots
