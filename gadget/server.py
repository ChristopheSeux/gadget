import socket
import pickle
from PySide2.QtCore import QThread, Signal, QObject
from PySide2.QtWidgets import QApplication

#from gadget.constants import APP

import socket
import threading

import gadget


class ClientThread(QThread):
    closed = Signal()

    def __init__(self, client):
        super().__init__()

        self.client = client

        self.address = client.address
        self.sock = client.sock
        self.buffer_size = client.buffer_size

        self.filepath = None
        self.version = None
        self.data = {}
        self.entity = None
        self.versions = []

    def send(self, msg) :
        d = pickle.dumps(msg)
        self.sock.sendall(d)

        print('[SERVER] Message sent', msg)

    def receive(self) :
        msg = b''
        while self.isRunning :
            try :
                print('[SERVER] Waiting for client input')
                chunk = self.sock.recv(self.buffer_size)
            except Exception as e :
                #print(e)
                self.closed.emit()
                break

            if chunk == b'' :
                self.closed.emit()
                break

            msg+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        if msg :
            return pickle.loads(msg)

    def run(self):
        p = gadget.context.project

        self.send(['GET', {'filepath': 'bpy.data.filepath'}])

        while self.isRunning:
            msg = self.receive()

            if not msg : break

            print('[SERVER] Data Receive from client :', msg)

            if msg == 'Exit' :
                self.closed.emit()

            elif isinstance(msg, (list, tuple)) and len(msg) == 2 :
                action_name, data = msg
                if action_name == 'COMMENT' :
                    versions = [v for v in self.versions if v.task_name == data['task']]
                    if versions :
                        version = versions[0]
                        version.publish(status=data['status'], comment = data['comment'])
                        version.task.update()
                    #print('Comment')

                elif action_name == 'PLAYBLAST' :
                    version = self.versions[0]
                    version.playblast(file=data['file'], add_to_kitsu=data['add_to_kitsu'], play_on_finish= data['play_on_finish'])
                    #action = APP.project.shots.actions[data['name']]
                    #del data['name']
                    #action.run(**data)
                    #print('Comment')

                elif action_name == 'POST' :
                    self.data.update(data)

                    if 'filepath' in data  :
                        path = data['filepath']
                        #if not path : break

                        self.entity = None
                        versions = []
                        # Set entity if found from path

                        #print('\nFILEPATH', path)
                        #print('\nTEMPLATE', gadget.app.project.templates['asset_file'].string)
                        
                        d = p.templates['asset_file'].parse(path)
                        if d :
                            self.entity = p.assets.get(d['name'])

                        # It's not an asset path maybe it's a shot :
                        if not self.entity :
                            d = p.templates['shot_file'].parse(path)
                            if d :
                                self.entity = p.shots.find(number= d['shot'],
                                sequence_number=d['sequence'])

                        if self.entity :
                            #print('#### ENTITY FOUND FROM PATH', self.entity)
                            self.versions = []
                            for task in self.entity.tasks :
                                task.set_versions()
                                v = task.versions.find(version=d['version'])
                                if v :
                                    self.versions.append(v)

                            #print('VERSIONS', versions)
                            if self.versions :
                                data = {
                                    'template_file' : self.entity.templates['file'].string,
                                    'template_output' : self.entity.templates['output'].string,
                                    'screenshot' : self.versions[0].screenshot,
                                    'file_infos' : str(self.versions[0].file_infos),
                                    'entity_type' : self.entity.type.norm_name,
                                    'task_items' : [v.task.name for v in self.versions],
                                    'all_status_items' : [s.norm_name for s in p.task_statuses],
                                    'artist_status_items' :[s.norm_name for s in p.task_statuses.find_all(is_artist_allowed=True)]
                                }

                                self.send(['POST', data])

                    gadget.app.signals.connection_updated.emit(self)

                '''
                # Find related version
                self.entity = None
                if data[0] == 'filepath' :

                    template_data = None
                    try :
                        d = self.app.project.templates['asset_file'].parse(data[1])
                        self.entity = self.app.project.assets[d['name']]
                        #version =
                    except :
                        d =self.app.project.templates['shot_file'].parse(data[1])
                        '''

        print('\nClosing client')


class Client(QObject):
    def __init__(self, server, sock, address):
        super().__init__()

        self.sock = sock
        self.address = address
        self.buffer_size = 4096

        #self.signal = True
        self.server = server

        self.client_thread = None

    def __str__(self):
        return str(self.address)

    def start(self):
        self.client_thread = ClientThread(self)
        self.client_thread.closed.connect(lambda:self.server.remove_client(self) )
        self.client_thread.start()


class ServerThread(QThread):
    client_connected = Signal(object, object)

    def __init__(self, server):
        super().__init__()
        
        self.server = server
        self.sock = server.sock

    def run(self):
        while self.isRunning :


            try :
                sock, address = self.sock.accept()
            except :
                break

            
            print('#### sock, adress', sock, address)

            self.client_connected.emit(sock, address)

            #client = Client(self, sock, address)
            #self.connections.append(client)
            #client.start()
            #QApplication.instance().signal.connection_updated.emit()

            #print("[SERVER] New connection " + str(client))

        print('[SERVER] Server Thread terminate')


class Server(QObject) :
    def __init__(self, app) :
        super().__init__()
        #Variables for holding information about connections
        self.clients = []

        print('Start Server Thread')
        #Get host and port
        self.host = socket.gethostname()
        self.port = 8888

        #Create new server socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #try :
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        self.server_thread = None

        app.signals.closed.connect(self.stop)

    def start(self):
        self.server_thread = ServerThread(self)
        self.server_thread.client_connected.connect(self.add_client)
        self.server_thread.start()

    def add_client(self, sock, address):
        #pass
        client = Client(self, sock, address)
        client.start()

        self.clients.append(client)

    def remove_client(self, client):
        if client not in self.clients:
            return

        self.clients.remove(client)

        try :
            client.sock.shutdown(socket.SHUT_RDWR)
            client.sock.close()
        except Exception as e:
            print(e)
            #print('[SERVER] The connection was already closed')

        print(f'[SERVER] The server is closing the connection with client {client.address}')
        client.client_thread.isRunning = False
        client.client_thread.quit()
        #client.client_thread.wait()
        

        gadget.app.signals.connection_updated.emit(client)

    def stop(self) :
        print('Closing the connections')
        for c in self.clients:
            self.remove_client(c)
            #c.client_thread.wait()

        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()

        self.server_thread.isRunning = False
        self.server_thread.quit()
        #self.server_thread.wait()

'''
class ServerThread(QThread):
    def __init__(self):
        super().__init__()

        self.header_size = 10
        self.buffer_size = 4096
        self.is_running = True
        self.host = socket.gethostname()
        self.port = 8888
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        self.clients = []

        print('Doliprane Server initialysed')

    def receive_data(self, socket) :
        #socket.setblocking(0)
        data = b''
        while self.is_running :
            try :
                print('Receiving from client')
                chunk = socket.recv(self.buffer_size)
                print('chunk', chunk)
            except ConnectionResetError :
                print('remove client from list')
                break
            except  ConnectionAbortedError :
                print('Connexion Aborded client from list')
                break

            data+= chunk
            if not chunk or len(chunk) < self.buffer_size: break

        #socket.close()
        if data :
            return pickle.loads(data)

    def send(self, socket, message) :
        msg = pickle.dumps(message)
        socket.sendall(msg)

        print('Message sent')

    def run(self):
        while self.is_running :
            #Accept connection request.
            client, addr = self.sock.accept()

            if client not in self.clients :
                print('add client', client)
                self.clients.append(client)
                self.send(client, 'Welcome to Doliprane Server')

            for c in self.clients :
                data = self.receive_data(client)
                #print(data)
'''
