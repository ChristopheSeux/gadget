
from canevas import Collection
#from gadget.entities.episode import Episode


class Episodes(Collection):
    def __init__(self, projects) :
        super().__init__()

        #self.set()

    def set(self) :
        print('Set Episodes')
        self.clear()
        return [self.add(p) for p in self.project_list]

    def add(self, project):
        if not project or not project.exists() :
            print(f'The path of the project {project} does not exist')
            return

        p = Project.from_config(project)
        self[p.name] = p
        return p



class Episode:
    def __init__(self, data):
        self.name = data['name']