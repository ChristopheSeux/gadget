
import os
import sys

from canevas.collection import Collection
from pathlib import Path
from env import Env
import gadget

from gadget.entities.project import Project
#from gadget.collections.environments import Environments

from gadget.entities.entity import BaseEntity
#from gadget.base_objects.stylesheet import StyleSheet
#from gadget.settings import Settings

from gadget.constants import GADGET_DIR

#from canevas.application import QtApplication

class Environments(Collection):
    def __init__(self, envs) :
        super().__init__()

        for e in envs:
            self.add(e)

    def set(self) :
        print('Set Environments')
        for e in self:
            e.set()

    def add(self, env_file):

        #print('###', env_file)

        env = Env(env_file)
        self[Path(env_file).stem] = env

        #print(env)

        return env


class Studio(BaseEntity):
    def __init__(self, data):
        

        self.project = None

        python_paths = os.environ.get('PYTHONPATH','').split(os.pathsep)
        os.environ['PYTHONPATH'] = os.pathsep.join(python_paths+[str(GADGET_DIR)] )
        os.environ['GADGET_DIR'] = str(GADGET_DIR)

        #self.set_envs()
        self.environments = Environments(data['environments'])
        self.environments.set()

        #print()
        #print('STUDIO_PIPE', os.getenv('STUDIO_PIPE'))
        #print('TEMPLATES_DIR', os.getenv('TEMPLATES_DIR'))

        self.data = self.expand_data(data)


        #self.tracker = data['tracker']
        #self.settings = Settings(self)
        #self.context = Context(self)



        gadget.studio = self
        #gadget.settings = self.settings

        sys.modules.update( {
            'gadget.studio' : self,
        } )

        #self.main_window = MainWindow(self)

    def set_project(self, project_key):
        project_conf = self.data['projects'][project_key]
        if not project_conf or not project_conf.exists() :
            print(f'The path of the project {project_conf} does not exist')
            return

        self.project = Project.from_config(project_conf)

    @classmethod
    def from_config(cls, config):

        data = {
            'projects': [],
            'environments': []
        }

        #print('config', config, cls.read_config(config))
        data.update(cls.read_config(config, expand_vars=False, obj_from_path=False) or {})
        
        return cls(data)



