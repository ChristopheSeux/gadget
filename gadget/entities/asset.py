
from gadget.entities.entity import Entity, Entities


class Assets(Entities) :
    def __init__(self, project) :
        super().__init__( project, project.data.get('assets', {}) )

        #self.project = project
        #tpl_moodboard_name = self.conf.get('thumbnail_moodboard')
        #self.templates['moodboard'] = project.templates.get(tpl_moodboard_name)

    def set(self, datas) :
        print('Set Assets')

        self.clear()
        assets = [self.add(data) for data in datas ]

    def add(self, data) :
        asset = Asset(self, data)
        self[asset.name] = asset

        return asset

    def new(self, name, asset_type, description='', data={}) :
        asset_data = self.project.tracker.new_asset(
            name = name,
            asset_type = asset_type.id,
            description = description,
            data = data
        )
        asset_data['asset_type_id'] = asset_type.id

        return self.add(asset_data)


class Asset(Entity) :
    def __init__(self, assets, data) :
        super().__init__(assets, data)

        #print(data)
        self.is_shot = False

        type_id = data.get('asset_type_id') or data.get('entity_type_id')
        self.type =  self.project.entity_types.get_by_id(type_id)

        #print(self.type)

        #self.tpl_moodboard = self.collection.tpl_moodboard

        self.label = self.type.name+' / '+self.name

        self.tasks = Tasks(self)


    @property
    def format_data(self) :
        name = self.norm_name.split('/')[0]
        entity_type = self.type.norm_name
        return dict(name=name, asset_type=entity_type)

    def to_dict(self) :
        return dict(
            name = self.name,
            norm_name = self.norm_name,
            description = self.description,
            project_id = self.project.id,
            id = self.id
        )

    def to_str(self) :
        return json.dumps(self.to_dict(), separators=(',',':'))


    def remove(self) :
        super().remove()

        # Remove asset from kitsu
        gazu.asset.remove_asset(self.data, force=True)


class AssetCasting:
    def __init__(self, project, data) :
        super().__init__()

        self.project = project
        self.data = data
        self.nb_occurences = data['nb_occurences']

        self.asset = self.project.assets.get_by_id(data['asset_id'])