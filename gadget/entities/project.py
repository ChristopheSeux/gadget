

import os
from tpl import Templates
from env import Env


import gadget
#from gadget.constants import *
from gadget.entities.entity import BaseEntity
from gadget.entities.soft import Softs
from gadget.entities.person import Persons
from gadget.entities.episode import Episodes
from gadget.entities.task_status import TaskStatuses
from gadget.entities.task_type import TaskTypes
from gadget.entities.asset import Assets
from gadget.entities.shot import Shots
from gadget.entities.sequence import Sequences
from gadget.entities.entity_type import EntityTypes


class Project(BaseEntity):
    def __init__(self, data) :
        super().__init__()

        self.data = data
        self.id = None

        self.name = data['name']
        self.icon = Path(data['icon']) if data['icon'] else None
        self.user_id = None
        self.type = data['type']

        print('PROJECT SET ENV', *data['envs'])
        self.env = Env(*data['envs'])
        #os.environ['DLP_ENV'] = os.pathsep.join(envs)
        self.env.set()
        #self.env_props = PropertyGroup.from_dict(self.env.data)

        #templates = conf.get('templates')

        self.templates = Templates(data['templates'])

        self.work_in_local = data['work_in_local']
        self.local_root = data['local_root']

        self.type = data['type']

        #self.load_prefs()
        self.episodes = Episodes(self)
        #self.set_env()
        #self.set_local_template()

        #self.set_actions()

        #self.settings = QSettings(self.name, 'doliprane')
        #self.restore_settings()

        #print('\n>>>', data['tracker'])
        self.tracker = data['tracker']()

        #os.environ['TEMPLATES'] = os.pathsep.join( [i.as_posix() for i in self.conf['templates'] ] )

        self.softs = Softs(self, data['softs'] )
        self.persons = Persons(self)
        self.task_statuses = TaskStatuses(self)
        self.assets = Assets(self)
        self.shots = Shots(self )
        self.sequences = Sequences(self )
        self.entity_types = EntityTypes(self)
        self.task_types = TaskTypes(self)

    @classmethod
    def from_config(cls, config):

        data = {
            'name': '',
            'tracker': '',
            'icon': None,
            'type': '',
            'envs': [],
            'templates': [],
            'work_in_local': False,
            'local_root': '',
            'softs': [],
            'episodes': []
        }

        data.update(cls.read_config(config))
       
        #for k, v in data.items():
        #    print(k, v)

        return cls(data)

    def set(self):
        pass

    def set_data(self, data) :
        #print('Set Project data\n', data)
        self.id = data['id']


    def set_env(self, root=None) :
        print('Set Env', root)
        if not root :
            root = self.prefs.local_root

        self.env['LOCAL_ROOT'] = root
        self.env.set()

    def set_local_template(self, root=None) :
        print('Set Local template', root)
        if not root :
            root = self.prefs.local_root

        if not root :
            self.local_templates = self.templates.copy()
            return
            #return self.templates.copy()

        local_templates = deepcopy(self.templates.raw)
        types = deepcopy(self.templates.types)

        local_templates['root'] = root

        self.local_templates = Templates(local_templates, types=types )


class TVShowProject(Project):
    pass

class FeatureFilmProject(Project):
    pass

class ShortProject(Project):
    pass