
#from PySide2.QtGui import QColor

import gadget
from canevas import Collection
from gadget.entities.entity import BaseEntity
from gadget.base_objects.field import Color


class Persons(Collection) :
    def __init__(self, project) :
        super().__init__()

        self.project = project

    def set(self, datas) :
        self.clear()

        persons = [ Person(self, p) for p in datas ]

        for p in persons :
            self[p.full_name] = p

        #gadget.app.signals.persons_updated.emit(self)


class Person(BaseEntity):
    def __init__(self, persons, data) :

        #print('PERSON')
        #print(data)

        self.parent = persons
        #print('')
        #print(data)
        #print('')
        self.role = data.get('role')
        self.project = persons.project

        self.data = data
        self.id = data['id']

        self.first_name = data['first_name']
        self.last_name = data['last_name']
        self.full_name = data['full_name']
        #self.role = data['role']
        self.email = data.get('email')

        self.color = Color(data.get('color'))
