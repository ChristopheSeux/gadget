
from gadget.entities.entity import Entity, Entities


class Sequences(Entities) :
    def __init__(self, project) :
        super().__init__( project, project.data.get('sequences', {}) )

    def set(self, sequences) :
        self.clear()
        print('Set Sequences')
        for s in sequences :
            #print(s)
            sequence = Sequence(self, s)
            self[sequence.name] = sequence
        gadget.app.signals.sequences_updated.emit(self)

    def add(self, sequence) :
        sequence = Sequence(self, sequence)
        self[sequence.name] = sequence

        gadget.app.signals.sequences_updated.emit(self)

        return sequence

class Sequence(Entity):
    def __init__(self, sequences, data) :
        super().__init__(sequences, data)

        self.type =  self.project.entity_types.get_by_id(data['entity_type_id'] )
        self.name = data['name']
        #print('Sequence Name', self.name)
        name_data = self.project.templates['sequence_name'].parse(self.name)
        self.number = name_data.get('sequence', -1)

        #self.tasks = ShotSeTasks(self, data.get('tasks', []))

    def to_dict(self) :
        return dict(
            id = self.id,
            name = self.name,
            type = self.type.name,
            number = self.number
        )