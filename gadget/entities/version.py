
from PySide2.QtWidgets import QApplication

from pathlib import Path
import shutil
import gazu
import json

#from gadget.base_objects.application import Application
from gadget.base_objects.collection import Collection
from gadget.base_objects.worker import Worker
#from gadget.base_objects.dependency import Dependency
from gadget.utils import copy, get_func_from_path, open_file
from gadget.constants import *
from os.path import abspath
import os


class Dependency(BaseEntity):
    def __init__(self, version, path):
        self.version = version
        self.relative_path = path
        self.local_path = Path( abspath( str(version.local_path.parent) + path ) )
        self.path = Path( abspath( str(version.path.parent) + path ) )
        self.type = 'FILE'

    def __repr__(self):
        return f'Dependency({self.relative_path})'


class Versions(Collection) :
    def __init__(self, project) :
        super().__init__()
        self.project = project

    def set(self, task, versions) :
        self.clear()
        for v in versions :
            self.add(task, v)
        #self.project.signals.assets_updated.emit()

    def add(self, task, version) :
        v = Version(task, version)
        self[v.name] = v

        #self.project.signals.assets_updated.emit()

        return v


class Version(BaseEntity) :
    def __init__(self, task, version) :
        self.task = task
        self.task_name = task.name
        self.task_type = task.task_type
        self.project = self.task.project
        self.soft = self.task_type.soft
        self.entity = self.task.entity
        self.templates = self.task.templates
        self.local_templates = self.task.local_templates

        self.version = version
        #print(self.entity.name)
        #print('self.path', self.path)
        #print('self.local_pat', self.local_path)
        container = self.project.env['REVIEW_CONTAINER']#, 'QUICKTIME')
        self.output_ext = 'mov' if container== 'QUICKTIME' else 'mp4'

        tpl = self.project.templates

        self.name = tpl['version_name'].format({'version': version })
        self.extension = self.task.format_data['ext']

        self.screenshot = self.task.templates['screenshot'].format(self.format_data)

        self.infos = {}
        self.dependencies = []

        self.on_local = self.local_path.exists()
        self.on_server = self.path.exists()

        #if self.file_infos.exists() :

        #self.frame_start
        #print('screenshot', self.screenshot)

        #self._playblast = self.conf.get('playblast')

        #if self._playblast and self._get_path.parent.suffix == '.py' :
        #    self._playblast = get_func_from_path(self._playblast.parent, self._playblast.stem)

    @classmethod
    def from_path(cls, path, project_name=None, entity_type='shot'):

        T = template.Templates()

        settings = Application.get_settings()

        if not project_name:
            project_name = os.environ('PROJECT_SHORT')

        project = Project.from_name(project_name)

        data = project.templates['shot_file'].parse(path)
        if data and 'shot' in data:
            task = ShotTask.from_name(data['shot_task'])
            task.entity = Shot.from_name()
        
        return cls(task, data['version'])

        # It's not an asset path maybe it's a asset :
        if not self.entity :
            d = p.templates['asset_file'].parse(path)
            if d :
                self.entity = p.shots.find(number= d['shot'],
                sequence_number=d['sequence'])


        pass

    def get_file_infos(self, force=False):
        if self.infos and not force: return

        infos = {}
        if self.file_infos.exists() :
            self.infos = json.loads( self.file_infos.read_text() )

            if 'libraries' in self.infos :
                self.dependencies = [Dependency(self, d) for d in self.infos['libraries'] ]

            del self.infos['libraries']

    @property
    def file_infos(self) :
        return Path( self.templates['infos'].format(self.format_data) )

    @property
    def output(self) :
        return Path( self.templates['output'].format(self.format_data, ext=self.output_ext) )

    @property
    def local_output(self):
        return Path( self.local_templates['output'].format(self.format_data, ext=self.output_ext) )

    @property
    def path(self):
        return Path( self.templates['file'].format(self.format_data) )

    @property
    def local_path(self):
        return Path( self.local_templates['file'].format(self.format_data) )

    @property
    def work_in_local(self) :
        return self.project.prefs.work_in_local

    @property
    def format_data(self) :
        return {**self.task.format_data, 'version' : self.version}

    def download(self) :
        print(f'Download version from {self.path} to {self.local_path}')
        #self.local_path.parent.mkdir(exist_ok=True, parents=True)
        #shutil.copy2(self.path, self.local_path)

        self.entity.download(versions=[self])

    def upload(self) :
        print(f'Upload version from {self.local_path} to {self.path}')
        #self.path.parent.mkdir(exist_ok=True, parents=True) # Create dir on network
        self.entity.upload(versions=[self])

        # Dependency

        #shutil.copy2(self.local_path, self.path)

    def to_dict(self) :
        path = self.local_path if self.work_in_local else self.path
        output = self.local_output if self.work_in_local else self.output

        self.get_file_infos()

        data = {
            'infos' : self.infos,
            'path': str(path),
            'task': self.task.name,
            'task_id': self.task.id,
            'output': str(output),
            'type': self.entity.type.name,
            'version' : self.version
        }

        #print('\n>>>>>>>> Version to dict', data)
        if data['type'] == 'Shot' :
            data['sequence'] = self.entity.sequence.number
            data['shot'] = self.entity.number

        return data


    def open(self) :
        # Check if file exist and build it if not existing
        #print(f'Open Task {self.name} from path {self.path}')

        env = { 'TEMPLATE': self.templates['file'].string }

        path = self.local_path if self.work_in_local else self.path

        print(f'Open Task {self.name} from path {path}')

        self.soft.versions.active.launch(file=path, env=env)

    def publish(self, status=None, comment='', preview=None) :
        if status :
            status = self.project.task_statuses[status]
        else :
            status = self.task.status

        c = gazu.task.add_comment(self.task.id, status.id, comment)

        if preview :
            preview = gazu.task.add_preview(self.task.id, c, preview)

        print(f'Publish Task {self.name}')
        pass