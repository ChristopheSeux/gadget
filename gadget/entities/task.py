
from PySide2.QtCore import QObject, Signal
from PySide2.QtGui import QImage

import template
import sys
import subprocess
import platform
from pathlib import Path
import shutil
import json
from os.path import exists

#from gadget.windows.open_task_dlg import OpenTaskDlg
import gadget
from gadget.base_objects.comment import Comments
from gadget.base_objects.version import Versions, Version
from gadget.base_objects.collection import Collection
from gadget.base_objects.properties import EnumProperty
from gadget.constants import *



class Task(BaseEntity) :
    #status_updated = Signal()
    #versions_updated = Signal()
    #screenshot_updated = Signal()
    #comments_updated = Signal(object)

    def __init__(self, data={}) :
        super().__init__()

        
        '''
        self.collection = collection
        self.entity = collection.entity

        #raise Exception('Stop')

        self.project = collection.project
        self.task_type = self.project.task_types.get_by_id(data['task_type_id'])

        self.data = data
        #print(json.dumps(data, indent=4))

        self.id = data['id']
        self.for_shots = self.task_type.for_shots
        self.priority = data['priority']

        self.status = self.project.task_statuses.get_by_id(data['task_status_id'])

        assignees = [self.project.persons.get_by_id(a) for a in data['assignees'] ]

        self.assignees = Collection.from_attr('full_name', assignees)

        self.versions = Versions(self.project)
        self.version = EnumProperty(name='Version')
        #self.local_versions = Collection()
        self.widgets = []
        self.comments = Comments(self)
        

        self.extension = self.task_type.extension
        self.name = self.task_type.name
        self.norm_name = self.task_type.norm_name
        self.soft = self.task_type.soft

        self.conf = self.task_type.conf

        self.templates = self.task_type.templates
        self.local_templates = self.task_type.local_templates

        self.screenshot = None
        '''

    @classmethod
    def from_path(cls, data): ### Implementation not finished
        if data['for_shots']:
            self = ShotTask.from_path(data)
        else :
            self = AssetTask.from_path(data)

        self.soft = Soft.from_name(data['soft'])
        self.task_types.from_name(data['task'])

        self.templates = template.Templates(data['templates'])
        self.local_templates = template.Templates(data['local_templates'])

        return self

    @classmethod
    def from_dict(cls, collection, data):
        self = cls(data)

        self.id = data['id']

        self.collection = collection
        self.entity = collection.entity

        self.project = collection.project
        self.task_type = self.project.task_types.get_by_id(data['task_type_id'])

        self.data = data

        self.for_shots = self.task_type.for_shots
        self.priority = data['priority']

        self.status = self.project.task_statuses.get_by_id(data['task_status_id'])

        assignees = [self.project.persons.get_by_id(a) for a in data['assignees'] ]

        assignees = [a for a in assignees if a is not None]

        self.assignees = Collection.from_attr('full_name', assignees)

        self.versions = Versions(self.project)
        self.version = EnumProperty(name='Version')
        #self.local_versions = Collection()
        self.widgets = []
        self.comments = Comments(self)

        self.extension = self.task_type.extension
        self.name = self.task_type.name
        self.norm_name = self.task_type.norm_name
        self.soft = self.task_type.soft

        self.build_script = self.task_type.build_script

        self.conf = self.task_type.conf

        self.templates = self.task_type.templates
        self.local_templates = self.task_type.local_templates

        self._format_data = self.conf.get('format_data', {})

        self.screenshot = None

        return self


    def set_screenshot(self) :
        if not self.versions : return
        version = self.versions[-1].version
        screenshot = self.templates['screenshot'].format(self.format_data, version=version)

        if not exists(screenshot):
            screenshot = self.templates['screenshot'].find(self.format_data, version=template.LAST)

        if screenshot and exists(screenshot):
            self.screenshot = QImage( Path(screenshot).as_posix() )
            self.screenshot_updated.emit()

    def output(self) :
        if self.versions : 
            return self.versions[-1].output()

    @property
    def file(self) :
        return Path( self.templates['file'].format(self.format_data) )

    def update(self):
        import gazu

        t = gazu.task.get_task(self.id)
        self.status = self.project.task_statuses.get_by_id(t['task_status_id'])
        self.status_updated.emit()
        pass

    def set_versions(self) :
        #print(self.tpl_local_file, self.format_data)

        local_versions = self.local_templates['file'].find_all(self.format_data, get='version')
        distant_versions = self.templates['file'].find_all(self.format_data, get='version')

        versions = list(set(local_versions+distant_versions))

        self.versions.set(self, sorted(versions) )

        self.versions_updated.emit()
        #print('update_versions')

    def insert_file(self, file) :
        copy = 'copy' if platform.system() == 'Windows' else 'cp'

        dst = self.templates['file'].find(self.format_data, version=template.LAST_NEXT)
        if not dst :
            dst = self.templates['file'].format(self.format_data, version=1)
        Path(dst).parent.mkdir(parents=True, exist_ok=True) # Create parent dir
        cmd = [copy, str(file), str(dst)]


        subprocess.call(cmd)
        self.set_versions()

    def open(self, version_name) :
        if version_name == 'None' :
            self.build(keep_open=True)
        else :
            active_version = self.task.versions[version_name]
            active_version.open()

    
    def build(self, background=False, local=None) :
        print(f'Build Task {self.name}')

        python_path = str(MODULE_DIR.parent)
        sys.path.append(python_path)

        if local is None :
            local = self.project.prefs.work_in_local

        tpl_file = self.local_templates['file'] if local else self.templates['file']

        file = Path(tpl_file.format( self.format_data, version=0 ) )
        file.parent.mkdir(exist_ok=True, parents=True)
        #file.touch()
        src_file = None
        if self.task_type.build_from :
            task_name = self.project.task_types[self.task_type.build_from].norm_name
            if self.task_type.for_shots :
                format_data = {**self.format_data, **{'shot_task': task_name} }
            else :
                format_data = {**self.format_data, **{'task': task_name} }

            src_file = Path(tpl_file.find( format_data, version=template.LAST ) )

        #version = self.task_type.soft_version
        cmd = self.soft.versions.active.get_cmd(file=src_file,  python=str(self.build_script),
        background=background, args={'--file':str(file), '--entity' : self.entity.to_str()} )

        print('cmd :', cmd)

        subprocess.Popen(cmd)

        self.set_versions()

    

class Tasks(Collection) :
    def __init__(self, entity) :
        super().__init__()

        self.entity = entity
        self.project = entity.project
        self.for_shots = entity.is_shot

        self.set(entity.data.get('tasks', []))

    def set(self, datas) :
        self.clear()
        tasks = [self.add(data, emit_signal=False) for data in datas ]

        gadget.app.signals.tasks_updated.emit(tasks)

    def add(self, data, emit_signal=True) :
        if self.for_shots :
            task = ShotTask.from_dict(self, data)
        else :
            task = AssetTask.from_dict(self, data)

        self[task.name] = task

        if emit_signal :
            gadget.app.signals.tasks_updated.emit([task])

        return task

    def new(self, task_type, task_status=None, assigner=None, assignees=[]):
        task = self.project.tracker.add_task(
            entity = self.id,
            task_type = task_type.id,
            task_status = task_status.id,
            assigner = assigner.id if assigner else None,
            assignees = [assignees.id for a in assignees] )

        self.add(task)

        return task

class AssetTask(Task) :
    @classmethod
    def from_dict(cls, collection, data):
        self = super().from_dict(collection, data)
        self.asset_tasks = collection

        return self

    @property
    def format_data(self) :
        return {**self.entity.format_data, 'ext': self.extension }

    @classmethod
    def from_path(cls, path):
        obj = cls(path) 


class ShotTask(Task) :
    @classmethod
    def from_dict(cls, collection, data):
        self = super().from_dict(collection, data)
        self.shot_tasks = collection

        return self

    @classmethod
    def from_path(cls, path):
        self = cls({}) 
        self.sequence = data['sequence']

        return self

    @property
    def format_data(self) :
        data = self.entity.format_data
        data.update({'shot_task': self.norm_name, 'ext': self.extension})
        data.update(self._format_data)

        return data

    def to_dict(self) :
        data = dict(
            id = self.id,
            entity = self.entity.to_dict(),
            name = self.name,
            sequence = self.entity.sequence.to_dict(),
            conf = {k: str(v) if isinstance(v, Path) else v for k,v in self.conf.items()},
            templates = self.templates.to_dict(),
            local_templates = self.local_templates.to_dict(),
            for_shots = True
        )

        return data

