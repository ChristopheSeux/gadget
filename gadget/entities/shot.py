
from gadget.entities.entity import Entity, Entities


class Shots(Entities) :
    def __init__(self, project) :
        super().__init__( project, project.data.get('shots', {}) )

    def set(self, datas) :
        print('Set Shots')

        self.clear()
        shots = [self.add(data) for data in datas ]

    def add(self, data) :
        shot = Shot(self, data)
        self[shot.name] = shot

        return shot

    def new(self, sequence, name, nb_frames=None, frame_in=None, frame_out=None, data={}) :
        shot_data = self.project.tracker.new_shot(
            name = name,
            sequence = sequence.id,
            nb_frames=nb_frames,
            frame_in=frame_in,
            frame_out=frame_out,
            description = description,
        )
        return self.add(shot_data)


class Shot(Entity):
    def __init__(self, shots, data) :
        super().__init__(shots, data)

        self.is_shot = True

        self.frame_in = data.get('frame_in', None)
        self.frame_out =  data.get('frame_out', None)

        #print([ (i.name, i.id) for i in self.project.entity_types])
        #print(data['entity_type_id'])

        self.type =  self.project.entity_types.get_by_id( data['entity_type_id'] )

        name_data = self.project.templates['shot_name'].parse(self.name)
        self.number = name_data.get('shot', -1)

        parent_id = data.get('sequence_id') or data.get('parent_id')
        self.sequence = self.project.sequences.get_by_id(parent_id)

        self.sequence_number = self.sequence.number

        self.label = self.sequence.name+' / '+self.name

        self.tasks = Tasks(self)
    #def add_task(self, task_data) :
    #    task = ShotTask(self, task_data)
    #    self.tasks[task.norm_name] = task

    @property
    def format_data(self) :
        data = dict( shot=self.number, sequence=self.sequence.number)
        return data

    def to_dict(self) :
        return dict(
            **self.format_data,
            name = self.name,
            number = self.number,
            frame_in = self.frame_in,
            frame_out = self.frame_out,
            id = self.id
        )

    def to_str(self) :
        return json.dumps(self.to_dict(), separators=(',',':'))


    def remove(self) :
        super().remove()

        # Remove asset from kitsu
        gazu.shot.remove_shot(self.data, force=True)
