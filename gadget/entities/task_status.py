

from canevas import Collection
from gadget.base_objects.field import Color
from gadget.entities.entity import BaseEntity


class TaskStatuses(Collection) :
    def __init__(self, project) :
        super().__init__()

        self.project = project
        self.conf = project.data.get('task_statuses', [])

        self.for_artists = Collection()

    @Collection.data.getter
    def data(self) :
        return self.for_artists._data + self._data

    def set(self, datas) :
        for d in datas :
            task_status = TaskStatus(self, d)
            col = self.for_artists if d['is_artist_allowed'] else self
            col[task_status.norm_name] = task_status

        #gadget.app.signals.task_statuses_updated.emit(self)

class TaskStatus(BaseEntity) :
    def __init__(self, project, data) :
        self.project = project
        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.short_name = data['short_name']
        self.norm_name = self.short_name.upper()

        self.is_artist_allowed = data['is_artist_allowed']
        self.is_client_allowed = data['is_client_allowed']
        self.is_done = data['is_done']
        self.is_reviewable = data['is_reviewable']

        color = data['color']
        if color == '#f5f5f5' :
            color = '#7F7F7F'

        self.color = Color(color)

        #print(data)
