

from canevas import Config, Collection

import gadget

import shutil
from pathlib import Path
import json
import os
import tpl
from tpl import Templates



class BaseEntity:
    def __init__(self):
        pass
        #self.config = Config

    @staticmethod
    def read_config(config, as_pathlib=True, expand_vars=True, obj_from_path=True):
        return Config.read(config, as_pathlib=as_pathlib, 
        expand_vars=expand_vars, obj_from_path=obj_from_path)

    @staticmethod
    def get_obj_from_path(path):
        return Config.get_obj_from_path(path)

    @staticmethod
    def expand_data(data, as_pathlib=True, expand_vars=True, obj_from_path=True):
        return Config.expand(data, as_pathlib=as_pathlib, 
        expand_vars=expand_vars, obj_from_path=obj_from_path)

    @staticmethod
    def copy_file(src, dst, only_new) :
        if (dst.exists() and only_new and dst.stat().st_mtime>=src.stat().st_mtime):
            return

        print(f'Copy file from {src} to {dst}')
        if platform.system() == 'Windows' :
            subprocess.call(['copy', str(src), str(dst)], shell=True)
        else :
            subprocess.call(['cp', str(src), str(dst)])

    @staticmethod
    def copy_dir(src, dst, only_new=True, excludes=[]) :
        src, dst = Path(src), Path(dst)

        if dst.is_dir() :
            dst.mkdir(exist_ok=True, parents=True)
        else :
            dst.parent.mkdir(exist_ok=True, parents=True)

        if src.is_file() :
            copy_file(src, dst, only_new)

        elif src.is_dir() :
            src_files = [f for f in src.rglob('*') if not any([e in str(f) for e in excludes]) ]
            dst_files = [dst/f.relative_to(src) for f in src_files]

            for src_file, dst_file in zip(src_files, dst_files)  :
                if src_file.is_dir():
                    dst_file.mkdir(exist_ok=True, parents=True)
                else :
                    copy_file(src_file, dst_file, only_new)


class Entity(BaseEntity):
    def __init__(self, collection, data) :
        super().__init__()

        self.collection = collection
        self.project = collection.project

        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.norm_name = self.project.templates.norm_str(self.name)
        self.description = data['description']

        conf = self.collection.conf
        #print('project.templates',  project.templates)
        self.templates = self.collection.templates
        self.local_templates = self.collection.local_templates

        self.casting = []
        #self.comments = data.get('comments', [])

        self.dependencies = []
        #self.tasks = Collection()
        #self.set_tasks( data.get('tasks', []) )

    def get_last_output(self):
        return self.templates['output'].find(self.format_data,
        shot_task=tpl.LAST, version=tpl.LAST)

    def set_dependencies(self) :
        if self.scene_info.exists() :
            scene_infos = json.loads( self.scene_info.read_text() )
            self.dependencies = scene_infos['libraries']

    def set_casting(self) :
        self.casting = [AssetCasting(self.project, a) for a in self.data['casting'] ]

    def get_all_versions(self) :
        return [v for t in self.tasks for v in t.versions]

    def get_dependencies(self, versions=[]) :
        dependencies = set()
        versions = versions or self.get_all_versions()

        file_infos = None
        for v in versions : # Not read json if not necessary
            #if v.file_infos != file_infos :
            v.get_file_infos()
            dependencies |= set( v.dependencies )

            file_infos = v.file_infos

        return list(dependencies)

    @property
    def scene_info(self) :
        return self.root/'scene_info.json'

    @property
    def local_output(self) :
        return Path( self.local_templates['output'].format(self.format_data) )

    @property
    def output(self) :
        return Path( self.templates['output'].format(self.format_data) )

    @property
    def local_root(self) :
        return Path( self.local_templates['root'].format(self.format_data) )

    @property
    def root(self) :
        return Path( self.templates['root'].format(self.format_data) )

    @property
    def thumbnail(self) :
        return Path( self.templates['thumbnail'].format(self.format_data) )

    @property
    def preview(self) :
        return Path( self.templates['preview'].format(self.format_data) )


    def upload(self, versions=[]) :
        self.root.mkdir(exist_ok=True, parents=True)

        if self.local_root.exists() :
            print(f'Upload version from {self.local_root} to {self.root}')

            excludes = ['old','_old']
            if versions :
                excludes+= [v.path.name for v in self.get_all_versions() if
                v not in versions ]

            self.copy_dir(self.local_root, self.root, only_new=True, excludes=excludes)

            # Dependencies are relative for now
            for d in self.get_dependencies(versions=versions) :
                if d.path.exists() :
                    self.copy_dir(d.local_path, d.path, only_new=True)

        else :
            print(f'The directory {self.root} does not exist')


    def remove(self) :
        if self.local_root.exists() :
            print('Remove Local Entity', self.local_root)
            shutil.rmtree( str(self.local_root) , ignore_errors=True)

        if self.root.exists() :
            print('Remove Entity', self.root)
            shutil.rmtree( str(self.root) , ignore_errors=True)


    def download(self, versions=[]) :
        self.local_root.mkdir(exist_ok=True, parents=True)

        if self.root.exists() :
            print(f'Download version from {self.root} to {self.local_root}')

            for v in versions :
                self.copy_dir(v.path, v.local_path, only_new=True)

            for d in self.get_dependencies(versions=versions) :
                if d.path.exists() :
                    self.copy_dir(d.path, d.local_path, only_new=True)

        else :
            print(f'The directory {self.root} does not exist')


    def set_preview(self, image_path) :
        print(f'Set Preview to {self.thumbnail}')

        self.thumbnail.parent.mkdir(exist_ok=True, parents=True)

        size= QSize(150, 100)

        image = QImage( Path(image_path).as_posix() )
        if QColorSpace :
            image.convertToColorSpace(QColorSpace.SRgb)
        image.save(self.preview.as_posix() )

        image = image.scaled(size, Qt.KeepAspectRatioByExpanding, Qt.SmoothTransformation)
        image.save(self.thumbnail.as_posix() )

        self.preview_updated.emit()

        #shutil.copy2(image_path, self.thumbnail)


class Entities(Collection) :
    def __init__(self, project, conf) :
        super().__init__()

        self.project = project
        self.conf = conf

        '''
        self.templates = Templates({}, types=project.templates.types)
        self.local_templates = Templates({}, types=project.templates.types)

        for k, v in conf.get('templates', {}).items() :
            self.templates[k] = project.templates.get(v)
            self.local_templates[k] = project.local_templates.get(v)

        print('Entities Templates')
        print(self, self.templates)

        '''