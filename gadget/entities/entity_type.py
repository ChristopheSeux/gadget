
import gadget
from canevas import Collection


class EntityTypes(Collection) :
    def __init__(self, project) :
        super().__init__()

        self.project = project
        self.conf = project.data.get('softs', [])

        self.for_assets = Collection()

    @Collection.data.getter
    def data(self) :
        return self._data+ self.for_assets._data

    def set(self, datas) :
        self.clear()
        #self['Asset'] = EntityType(self, {'id' : None, 'name': 'Asset'})
        #data.append( {'id' : None, 'name': 'Asset'} )
        entity_types = [self.add(data) for data in datas ]

        #gadget.app.signals.entity_types_updated.emit(entity_types)

    def add(self, data, emit_signal=True) :
        if data['name'] in ('Shot', 'Sequence', 'Episode', 'Scene') :
            collection = self
            for_assets = False
        else :
            collection = self.for_assets
            for_assets = True

        entity_type = EntityType(self, data)
        entity_type.for_assets = for_assets
        collection[entity_type.name] =  entity_type

        #if emit_signal :
        #    gadget.app.signals.entity_types_updated.emit([entity_type])

        return entity_type


class EntityType:
    def __init__(self, entity_types, data) :

        self.parent = entity_types
        self.entity_types = entity_types

        self.project = entity_types.project
        self.data = data
        self.id = data['id']
        self.name = data['name']
        self.norm_name = self.project.templates.norm_str(self.name)

        self.for_assets = False

    def to_dict(self) :
        return {'name': self.name, 'id': self.id}
