
from pathlib import Path

from gadget.constants import *
from gadget.entities.entity import BaseEntity
from canevas import Collection


class TaskType(BaseEntity) :

    def __init__(self, task_types, data, conf) :

        super().__init__(conf)

        print('TaskType', self.conf)

        self.name = data['name']

        self.is_configured = True
        self.collection = task_types
        self.task_types = task_types

        self.project = task_types.project
        self.data = data

        if not self.conf :
            print(f'The task {self.name} is not configured')
            self.is_configured = False
            

        #print('\n>>> New Task Type :', self.name)

        self.norm_name = self.project.templates.norm_str(self.name)
        self.for_entity = data['for_entity']
        self.for_shots = data['for_shots']
        self.color = QColor(data['color'])

        self.build_script = self.conf.get('build')
        self.build_from = self.conf.get('build_from')

        if self.for_shots :
            self.entities = self.project.shots
        else :
            self.entities = self.project.assets

        self.id = data['id']
        #self.task_types = task_types
        self.priority = data['priority']

        soft = self.conf.get('soft','default')
        self.soft = self.project.softs[soft]
        self.soft_version = self.conf.get('soft_version', None)

        self.extension = self.soft.extension
        self.work_in_sequence = self.conf.get('work_in_sequence', False)

        self.templates = self.entities.templates.copy()
        self.local_templates = self.entities.local_templates.copy()

        for k, v in conf.get('templates', {}).items() :
            self.templates[k] = project.templates.get(v)
            self.local_templates[k] = project.local_templates.get(v)

    def to_dict(self):
        return dict(
            conf = {k: str(v) if isinstance(v, Path) else v for k,v in self.conf.items()},
            soft = self.soft.name
        )

class TaskTypes(Collection) :
    def __init__(self, project) :
        super().__init__()

        self.project = project
        self.for_assets = Collection()
        self.for_shots = Collection()

    @Collection.data.getter
    def data(self) :
        return self.for_assets._data + self.for_shots._data

    def set(self, datas) :
        #print('\n######', datas)

        datas.sort(key=lambda x :x['priority'])

        for data in datas :
            task_name = data['name']

            if data['for_shots'] :
                conf = self.project.conf['shots'].get('tasks', [])
                collection = self.for_shots
            else :
                conf = self.project.conf['assets'].get('tasks', [])
                collection = self.for_assets

            task_conf =  next((t for t in conf if t['name']==task_name), {})
            #if task_conf :
            collection[task_name] = TaskType(self, data, task_conf)
            #else :
            #    print(f'The task {task_name} is not configured')

        #gadget.app.signals.task_types_updated.emit(self)
