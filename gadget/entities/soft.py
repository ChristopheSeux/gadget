
import subprocess
import sys
import os
from pathlib import Path
import shutil

import tpl
from env import Env
import gadget
from gadget.entities.entity import BaseEntity

from canevas.collection import Collection
#from gadget.base_objects.config import Config


class SoftVersions(Collection):
    def __init__(self, soft, versions=[]):
        super().__init__()

        self.soft = soft
        self.set(versions)

    def add(self, *versions):
        soft_versions = []
        for version in versions:
            if isinstance(version, str):
                version_name = version
                version = SoftVersion(self.soft, version_name)
            else:
                version_name = version.name

            self[version_name] = version
            soft_versions.append(version)

        if len(versions) == 1:
            soft_versions = soft_versions[0]

        return soft_versions

    def set(self, versions=[]):
        self.clear()
        tpl_archive = self.soft.templates.get('archive')
        if tpl_archive:
            versions += tpl_archive.find_all({}, get='version')

        versions = sorted( list(set(versions)) )

        for v in versions:
            self.add(v)


class SoftVersion:
    def __init__(self, soft, name):
        self.soft = soft
        self.name = name

    def launch(self, file=None, env={}, **kargs):
        self.soft.launch(self, file=file, env=env, **kargs)

    def install(self):
        self.soft.install(self)

    def uninstall(self):
        self.soft.uninstall(self)


class Softs(Collection) :
    def __init__(self, project, datas=[]) :
        super().__init__()

        self.project = project
        self.set(datas)

    def set(self, datas):
        self.clear()

        softs = [ self.add(d) for d in datas  ]

        #gadget.app.signals.softs_updated.emit(softs)

    def add(self, soft):
        if isinstance(soft, (str, Path, dict)) :
            soft = Soft.from_config(soft)
        else:
            #print('Soft', soft)
            soft = soft()

        soft.softs = soft.collection = self

        self[soft.name] = soft

        #if emit_signal :
        #    gadget.app.signals.softs_updated.emit([soft])

        return soft


class Soft(BaseEntity):
    def __init__(self):
        super().__init__()

        self.name = ''
        self.hide = False
        self.icon = None
        self.extension = ''
        self.env = Env({})
        self.install_dir = None
        self.templates = tpl.Templates({})

        self.versions = SoftVersions(self, [])

    @classmethod
    def from_dict(cls, data):
        #self.data = data

        self = cls()

        self.name = data.get('name', self.name)
        self.hide = data.get('hide', self.hide)
        self.icon = data.get('icon', self.icon)
        self.extension = data.get('extension', self.icon)
        self.install_dir = data.get('install_dir', self.install_dir)

        if data.get('env'):
            self.env = Env(data['env'])

        if data.get('templates'):
            self.templates = tpl.Templates(data['templates'])

        #self.versions = SoftVersions(self)
        self.versions.set(data.get('versions', []))


        '''
        if data.get('install_func'):
            self.install_func = Config.get_obj_from_path(data['install_func'])

        if data.get('get_path_func'):
            self.get_path_func = Config.get_obj_from_path(data['get_path_func'])

        if data.get('get_cmd_func'):
            self.get_cmd_func = Config.get_obj_from_path(data['get_cmd_func'])
        '''

        return self

    @classmethod
    def from_config(cls, config):
        return cls.from_dict(cls.read_config(config) or {})

    @classmethod
    def from_name(cls, name):
        # Read the env
        project_config = os.getenv('PROJECT_CONFIG')

        if name not in project_config['softs']:
            print(f'The soft {name} is not in the config {project_config}')

        return cls(project_config['softs'][name])

    def get_version_name(self, version):
        return Path(self.templates['version_name'].format(version=version.name))

    def get_archive(self, version):
        return Path(self.templates['archive'].format(version=version.name))

    def get_path(self, version):
        return Path(self.templates['bin'].format(version=version.name))

    #@classmethod
    #def from_config(cls, config):
    #    return cls(Config.read(config))
    '''
    def get_active_version(self, version=None):
        if version is None:
            version = self.versions.active.name
        
        if version not in self.versions:
            raise Exception(f'The version {version} is not in {self.versions.keys()}')

        return self.versions[version]
        '''

    def get_cmd(self, version, file=None, **kargs) :

        cmd = [self.get_path(version)]

        if file :
            cmd += [str(file)]

        for k, v in kargs.items():
            cmd += [k]
            if isinstance(v, (tuple, list)) :
                cmd += v
            else :
                cmd += [v]

        return cmd

    def launch(self, version, file=None, env={}, **kargs):
        version = self.versions[version]
        for k, v in os.environ.items():
            print(k, v)

        self.env.set()

        cmd = self.get_cmd(version, file=file, **kargs)
        subprocess.Popen(cmd, env={**os.environ, **env})

    def install(self, version):
        print('\nInstall', self.name, version.name)

        #if self.install_script:
        #    print('Custom Installer Script', self.install_script)
        #    subprocess.call([sys.executable, str(self.install_script), '--version', version])
        #else: #Default install copy, unzip and rename
        

        src = self.get_archive(version)
        dst = self.get_root(version)

        if dst.exists():
            shutil.rmtree(dst)

        all_softs = set( dst.parent.glob('*') )

        print(f'Copy from {src} to {dst}')
        copy(src, dst)

        #extract_dir = Path(tempfile.tmpdir() ) / src.name
        print(f'Unzip {dst}')
        #unzip(dst, self.softs.root)
        shutil.unpack_archive(dst, dst.parent)
        #unzip(dst, self.softs_dir)

        print('Remove Zip')
        os.remove(dst)

        new_softs = list( set( dst.parent.glob('*') ) - all_softs )
        
        if len(new_softs) == 1:
            new_soft = new_softs[0]
            if new_soft != dst:
                print(f'Rename Soft {new_soft} to {dst}')
                shutil.move(new_soft, dst)
        

        print(self.name, version.name, 'is installed')

    def uninstall(self, version):
        print('\nUninstall ', self.name, version.name)
        shutil.rmtree(version.root)
        print(self.name, version.name, 'is removed')

    def __repr__(self):
        text = '\nSoft(\n'
        text += f'   name: {self.name}\n'
        text += '   versions: '
        if self.versions:
            text += '\n'
            for v in self.versions:
                text += f'      - {v.name}\n'
        else :
            text += '[]\n'
        
        text += ')\n'

        return text