
from datetime import datetime
from canevas.collection import Collection


class Comments(Collection) :
    def __init__(self, task) :
        super().__init__(self)

        self.task = task

    def set(self, datas) :
        self.clear()

        #datas = sorted(datas, key= lambda x : x.created_at)
        comments = [self.add(data, emit_signal=False) for data in datas ]

        self.task.comments_updated.emit(comments)

    def add(self, data, emit_signal=True) :
        comment = Comment(self, data)
        self[''] = comment

        if emit_signal :
            self.task.comments_updated.emit([comment])

        return comment

    def new(self):
        "add a comment to kitsu"
        pass

class Comment :
    def __init__(self, comments, data) :

        self.comments = comments
        self.collection = comments

        self.task = comments.task
        self.text = data['text']
        self.id = data['id']
        self.created_at = datetime.strptime(data['created_at'], "%Y-%m-%dT%H:%M:%S")
