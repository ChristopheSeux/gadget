
from PySide2.QtCore import QObject, Signal
from canevas.collection import Collection


class Signals(QObject) :
    connected = Signal()

    progress_updated = Signal()
    softs_updated = Signal(Collection)
    persons_updated = Signal(Collection)
    task_types_updated = Signal(Collection)
    task_statuses_updated = Signal(Collection)
    tasks_updated = Signal(Collection)
    tasks_setted = Signal()
    types_setted = Signal()
    assets_updated = Signal(Collection)
    asset_tasks_updated = Signal(Collection)
    shot_tasks_updated = Signal(Collection)
    comments_updated = Signal(Collection)
    sequences_updated = Signal(Collection)
    shots_updated = Signal(Collection)
    entity_types_updated = Signal(list)
    asset_types_updated = Signal(Collection)
    actions_updated = Signal(list)
    commands_updated = Signal(list)
    interfaces_updated = Signal(list)

    closed = Signal()
    project_updated = Signal(object)
    project_setted = Signal()
    #projects_updated = Signal(Collection)
    console_updated = Signal()
    episodes_updated = Signal(Collection)

    #new_connection = Signal(object)
    connection_updated = Signal(object)
    about_to_quit = Signal()
