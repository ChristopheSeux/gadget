
#from gadget.base_objects.project import Project


class Context:
    def __init__(self, app):

        self.app = app
        self.settings = app.settings
        #self.settings = app.settings
        #self.signals = app.signals

    @property
    def progress(self):
        disk_worker = self.app.disk_worker
        network_worker = self.app.network_worker
        active_workers = [w for w in (disk_worker, network_worker) if w.progress > 0 ]
        len_workers = max(1, len(active_workers) )

        return (disk_worker.progress + network_worker.progress)/ len_workers

    @property
    def tab(self):
        return self.app.main_window.tabs.currentTab()

    @property
    def selected_versions_output(self):
        return [t.output for t in self.selected_versions]

    @property
    def selected_versions_path(self):
        return [t.path for t in self.selected_versions]

    @property
    def selected_last_versions_output(self):
        return [t.get_last_output() for t in self.selected_entities]

    @property
    def selected_entities(self):
        [t.entity for t in self.selected_tasks]

    @property
    def selected_tasks(self):
        mw = self.app.main_window
        if self.tab == 'My Task':
            return mw.my_task_tab.get_selected_tasks()
        elif self.tab == 'Entity Browser':
            return mw.entity_browser_tab.get_selected_tasks()

    @property
    def selected_versions(self):
        mw = self.app.main_window
        if self.tab == 'My Task':
            return mw.my_task_tab.get_selected_versions()
        elif self.tab == 'Entity Browser':
            return [t.versions[-1] for t in self.selected_tasks if t.versions]

    @property
    def project(self):
        return self.app.studio.project

    @property
    def templates(self):
        return self.project.templates

    @property
    def tracker(self):
        return self.project.tracker

    @property
    def env(self):
        return self.project.env

    @property
    def user(self):
        return self.project.persons.get_by_id(self.project.user_id)

    @property
    def task_statuses(self):
        return self.project.task_statuses

    @property
    def allowed_task_statuses(self):
        if self.user.role == 'user':
            return self.project.task_statuses.for_artists
        return self.task_statuses