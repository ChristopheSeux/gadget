
from PySide2.QtWidgets import QSizePolicy, QFileDialog, QHeaderView, QAbstractScrollArea, QLineEdit
from PySide2.QtCore import Qt, QObject, Signal
from PySide2.QtGui import QPixmap, QImage, QTextCursor, QTextCharFormat, QBrush

#from gadget.constants import *
from canevas.ui.base_widgets import *
from canevas.ui.properties import *
from canevas.ui.property_group import PropertyGroup

import re
import json
import sys
import subprocess
from pathlib import Path
#import inspect
#from docopt import docopt
import sys
import yaml


class EnvWidget(FWidget):
    def __init__(self, parent, env):
        super().__init__(parent, spacing=8, margin=8)

        #self.widgets = []
        self.properties = PropertyGroup.from_dict(env.to_dict(), parent=self)


        print('env', env.to_dict())
        #print(self.widgets)


class TasksToolBar(HWidget):
     def __init__(self, parent):
        super().__init__(parent, Background='highlight', margin=8)   

        self.assign = StringLineEdit(self, name='Assign', clearBtn=True, icon='user_32', 
        LabelFontWeight='bold', tag='Dark')

        self.assign_confirm = PushButton(self, icon='check_32', tag='Transparent', 
        cursor='PointingHandCursor')
        Frame(self, Size='very_small')
        #self.addStretch()

        #self.status_label = WidgetLabel(self, 'Status', align='Left')
        self.status = StringLineEdit(self, name='Status', clearBtn=True, icon='user', 
        LabelFontWeight='bold', tag='Dark')
        self.status_confirm = PushButton(self, icon='check_32', tag='Transparent', 
        cursor='PointingHandCursor')
        Frame(self, Size='very_small')
        #self.addStretch()

        self.action_enum = ComboBox(self, items=['Priority', 'Create', 'Delete'])
        self.action_confirm = PushButton(self, icon='check_32', tag='Transparent', 
        cursor='PointingHandCursor')

        Frame(self, Size='small')
        #self.addStretch()
        self.import_csv = PushButton(self, icon='import_32')
        self.export_csv = PushButton(self, icon='export_32')
        Frame(self, Size='small')
        #self.addStretch()
        self.add = PushButton(self, icon='plus_32')



class TaskHeaderWidget(PushButton):
    def __init__(self, task_type, parent=None):
        super().__init__(parent, Height=None, tag='TaskHeader')

        self.task_type = task_type

        c = task_type.color
        bg = c.darker(135)
        bg = ( bg.red(), bg.green(), bg.blue(), 50)
        #outline = ( color.red(), color.green(), color.blue() )
        #color = task_type.color.darker(135)
        #background = ( color.red(), color.green(), color.blue(), 50)
        #print(f'background: rgba{background}; border-color: {outline};')
        self.setStyleSheet(f'background: rgba{bg}; border-left-color: {c.name()};')
        self.setText(task_type.name)


class TasksTable(TableWidget):
    def __init__(self, parent, tasks_types):
        super().__init__(parent)

        self.task_types = tasks_types
        #self.tasks_widget = parent
       # self.setSizeAdjustPolicy(QAbstractScrollArea.Fixed)
        self.setAlternatingRowColors(True)

        #self.setFixedHeight(200)

        self.verticalHeader().hide()
        self.horizontalHeader().hide()

        self.horizontalHeader().setMinimumSectionSize(32)

        self.setShowGrid(False)

        self.setRowCount(4)
        self.setRowHeight(0, 32)
        self.setColumnCount(len(self.task_types)+2 )

        for i, label in enumerate(['Name', 'Description']):
            widget = PushButton(text=label, Height=None, tag='TaskHeader')
            item = self.addItem(0, i, label, widget=widget)
            item.setFlags(Qt.NoItemFlags)           
        
        for i, task_type in enumerate(tasks_types):
            widget = TaskHeaderWidget(task_type)
            item = self.addItem(0, i+2, label, widget=widget)
            item.setFlags(Qt.NoItemFlags)

        for i in range(1, 4):
            self.setRowHeight(i, 48)
            name = LineEdit(sizePolicy=('Expanding', 'Expanding'), Height=None, tag='Cell')
            name.setFixedWidth(128)
            description = PlainTextEdit(tag='Cell')
            description.setFixedWidth(128)
            
            item = self.addItem(i, 0, 'Name', widget=name)
            item.setFlags(Qt.NoItemFlags)  
            
            item =self.addItem(i, 1, 'Description', widget=description)
            item.setFlags(Qt.NoItemFlags)  
        #for t in self.task_types:
        #    self.addItem(1, 1, 'test', widget=PushButton(text='Toto'))

        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        #self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        #self.horizontalHeader().setSectionResizeMode(0, QHeaderView.)
        self.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)
        self.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        #self.resizeColumnToContents(0)
        #self.resizeColumnToContents(1)

    '''
    def sizeHintForColumn(self, index):
        width = (self.width()-1)/self.columnCount()#super().sizeHintForColumn(index)
        #width-=0.1

        if index == 0:
            width = max(54, width)
        elif index == 1: 
            width = max(112, width) 
        
        return  width
        # For Making label Row
        #self.resizeColumnToContents ( 1 ) 
        for i in range(5):
            item=QTableWidgetItem()
            item.setFlags(Qt.NoItemFlags)
            self.setItem(4, i, item)
        '''

        #table.horizontalHeader().setSectionResizeMode(1, QHeaderView.ResizeToContents)
        #table.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents)

class TasksWidget(VWidget):
    column_resized = Signal((int, float))
    def __init__(self, parent, tasks_types):
        super().__init__(parent)   

        self.tool_bar = TasksToolBar(self)
        #self.header = TasksTableHeader(self, tasks_types)    
        self.table = TasksTable(self, tasks_types)



class OutputText(QObject):
    updated = Signal(str)

    def __init__(self, connect=None):
        super().__init__()

        if connect:
            self.updated.connect(connect)

    def write(self, text):
        sys.__stdout__.write(text)
        self.updated.emit(str(text))

    def flush(self):
        sys.__stdout__.flush()

    def __del__(self):
        sys.stderr = sys.__stderr__


class OutputError(QObject):
    updated = Signal(str)

    def __init__(self, connect=None):
        super().__init__()
        if connect:
            self.updated.connect(connect)

    def write(self, text):
        sys.__stderr__.write(text)
        self.updated.emit(str(text))

    def flush(self):
        sys.__stderr__.flush()

    def __del__(self):
        sys.stdout = sys.__stdout__


class Console(PlainTextEdit):
    def __init__(self, parent=None):
        super().__init__(parent, readOnly=True, focus='NoFocus',
        tag='Console', Background='very_dark')


        self.setMinimumHeight(35)
        self.setViewportMargins(5, 2, 2, 0)
        #self.setCenterOnScroll(True)
        #self.resize(200, 35)
        sys.stdout = OutputText(connect=lambda x: self.add_text(x, QColor(220, 220, 220)))
        sys.stderr = OutputError(connect=lambda x: self.add_text(x, QColor(195, 75, 30)) )

    def add_text(self, text, color=None):
        cursor = self.textCursor()

        if color:
            format = QTextCharFormat()
            format.setForeground( QBrush( QColor( color ) ) )
            cursor.setCharFormat( format )

        cursor.insertText(text)
        cursor.movePosition(QTextCursor.End)

        self.setTextCursor(cursor)
        self.ensureCursorVisible()

    def resizeEvent(self, e):
        super().resizeEvent(e)
        self.ensureCursorVisible()
        #self.verticalScrollBar().setValue(self.verticalScrollBar().maximum() )
        

class Thumbnail(ImageLabel):
    def __init__(self, parent, image=None, size=(48,32) ):
        super().__init__(parent, tag='Thumbnail')

        self.setFixedSize(*size)
        self.set_pixmap(image)

    def set_pixmap(self, image):
        pix = None
        if isinstance(image, (str, Path) ):
            image_path = Path(image)
            if image_path.exists():
                pix = QPixmap(image_path.as_posix())
        elif isinstance(image, QImage):
            pix = QPixmap.fromImage(image)

        if pix:
            pix = pix.scaled(self.size(), Qt.KeepAspectRatioByExpanding, Qt.SmoothTransformation)
            self.setPixmap(pix)
        else:
            self.setPixmap(QPixmap())


class ContextWidget(VWidget):
    def __init__(self, argument, parent=None):
        super().__init__(parent, name=argument.name)

        #self.label_row = HWidget(self)
        #self.expand_btn = PushButton(self.label_row, text=argument.name,
        #icon='right_32', tag='Transparent')
        #self.expand_btn.setCheckable(True)

        #self.label = TextLabel(self.label_row, text=argument.name)
        #self.label_row.addStretch()

        #self.data_path_wgt = HWidget(self.label_row, Background='very_dark')
        self.header_lyt = HLayout(self)
        self.header_lyt.addStretch()
        self.context_btn = PushButton(self.header_lyt, text='  '+argument.context+'  ', 
        checkable=True, Background='very_dark')
    
        text = yaml.dump(argument.eval(), indent=4)

        #print('\nText', text)

        self.arg_text = PlainTextEdit(self, text=text, readOnly=True, 
        focus='NoFocus')    
        self.arg_text.hide()   

        self.context_btn.clicked.connect(self.on_expand_clicked)

    def on_expand_clicked(self) :
        value = self.context_btn.isChecked()
        #self.expand_btn.setIcon( Icon('down_32' if value else 'right_32') )
        self.arg_text.setVisible(value)


class CommandWidget(FWidget):
    def __init__(self, command, parent=None):
        super().__init__(parent, spacing=5, margin=8)

        self.command = command
        self.name = command.name
        self.arguments = command.arguments

        self.widgets = []
        for arg in command.arguments: # PropertyGroup Maybe?
            if arg.hide:
                continue
            #text = '' if arg.type is bool else arg.name
            widget = arg.to_widget(self)
            #self.addRow(text,  widget)
            self.widgets.append( widget )


    #def get_args(self):
    #    return {k.norm_name: w.value for k,w in zip(self.arguments, self.widgets)}


class ActionPanel(VWidget):
    def __init__(self, action, parent=None):
        super().__init__(parent, Background='dark')

        self.action = action
        self.commands = action.commands

        self.title_row = HWidget(self, margin=6, Background='medium')
        self.enabled_cbox = CheckBox(self.title_row, name=self.action.label,
        icon=action.icon, direction='LeftToRight', FontSize='big')

        #Frame(self.title_row, tag='small')
        #self.label = TextLabel(parent=self.title_row, text=conf['name'])
        self.title_row.addStretch()

        self.stacked_wgt = StackedWidget(self, sizePolicy=('Expanding', 'Maximum'))
        self.enabled_cbox.stateChanged.connect(lambda x : self.stacked_wgt.setVisible(x))
        self.enabled_cbox.setChecked(self.action.enabled)

        if len(self.commands) >1:
            self.commands_enum = ComboBox(items=self.commands.keys(), 
            parent=self.title_row, connect=self.set_active_command)

        #print('Command Panel', self.commands)
        for command in self.commands:
            c = command.to_widget(self.stacked_wgt)#CommandWidget(command, parent=self.stacked_wgt)
            
            #print(c.isVisible() )
            #c.setVisible(False)
        #print('\n//////////////', action.name, self.commands)
        if len(self.commands) :
            self.set_active_command(self.commands[0].name)
        else :
            print('The action', action.name, 'has no valid command')

    def get_active_command(self) :
        w = self.stacked_wgt.currentWidget()
        if w :
            return w.command

    def is_enabled(self):
        return self.enabled_cbox.isChecked()

    def set_active_command(self, cmd_name):
        cmd_wgt = next(w for w in self.stacked_wgt.widgets() if w.name==cmd_name )
        #print(c.isVisible() )
        self.stacked_wgt.setCurrentWidget(cmd_wgt)
        self.enabled_cbox.setToolTip(cmd_wgt.command.path.as_posix())
        

    def get_command_widget(self):
        return self.stacked_wgt.currentWidget()


class ActionButton(PushButton):
    def __init__(self, action, parent=None):
        super().__init__(parent, text=action.label, icon=action.icon)

        self.action = action
        self.clicked.connect(self.run)

    def run(self):
        for cmd in self.action.commands :
            cmd.run()


class ActionMenu(Menu):
    def __init__(self, action, parent=None):
        super().__init__(parent)

        self.action = action

        for a in action.actions :
            self.addWidget(a.to_widget())
        #self.clicked.connect(lambda : print('Run action', self.action))


class ActionDialog(VDialog):
    def __init__(self, action, parent=None):
        super().__init__( parent, title=action.name, icon=action.icon,
        Background='dark', spacing=1)

        self.action_widgets = []
        for a in action.actions :
            self.action_widgets.append( a.to_widget(self) )

        self.layout().addStretch()

        self.confirm_row = HWidget(self, spacing=5, margin=8)

        ok_btn = PushButton(self.confirm_row, text='OK', connect=self.valid)
        ok_btn.setAutoDefault(False)

    def valid(self):
        print('\nRun actions')

        # Get all actions values
        for a in self.action_widgets :
            #print(a.get_active_command().name)
            if not a.is_enabled(): continue

            #print(a.action.name)

            command = a.get_active_command()

            print(command.name)
            print( command.get_cmds())
            a.get_active_command().run()

        '''

        for p in self.panels:
            if not p.is_enabled(): continue

            action_widget = p.get_action_widget()
            args = action_widget.get_args()
            cmds = action_widget.action.get_cmds( **args )


            gadget.app.disk_worker.add_cmd( *cmds )
            #gadget.worker.add_cmd( *cmds )
        #for panel in self.panels:
        #
        #self.action.run(**args)
        '''
        self.close()
    


"""
class ActionButton(PushButton):
    def __init__(self, action, parent=None, icon=None, draw_text=False,
    cursor=None, toolTip='', tag=None, **kargs):

        super().__init__(parent=parent, text=action.name if draw_text else '',
        icon=icon, cursor=cursor, toolTip=toolTip, **kargs)

        self.action = action
        self.arguments = action.arguments
        self.path = action.path
        self.name = action.name
        self.icon = None

        self.dialog = VDialog(parent=parent, title=self.name, Spacing='normal', Padding='big',
        icon=self.icon)

        form_widget = FWidget(self.dialog, Spacing='normal')

        self.widgets = []
        for arg in self.arguments:
            if arg.data_path: continue
            text = arg.name
            if arg.type is bool:
                field = Bool(value=arg.default, name=arg.name)
                text = ''
            elif arg.type is str:
                print('>>> ARG', arg.name)
                field = String(value=arg.default)
            elif arg.type is int:
                field = Int(value=arg.default, decimals=0)
            elif arg.type is float:
                field = Float(value=arg.default)
            elif arg.type in (tuple, list):
                field = Enum(value=arg.default, items=arg.choices)

            form_widget.layout.row(text,  field)
            self.widgets.append( field )

        ok_btn = PushButton(parent=self.dialog, text='OK', connect=self.valid)
        ok_btn.setAutoDefault(False)

        self.clicked.connect(self.dialog.exec_)

    def valid(self):
        args = {k.norm_name: w.value for k,w in zip(self.arguments, self.widgets)}
        self.action.run(**args)

        self.dialog.close()

class PythonAction(WidgetAction):
    def __init__(self, menu, action):

        self.action = action
        self.arguments = action.arguments
        self.path = action.path
        self.name = action.name
        self.icon = None

        #### Test to read a data dict a the top of the python file
        '''
        self.script = Path(script_path).read_text()

        infos = re.findall(r' *?info *?= *?({[\w\n: _\.,\'\"{}\[\]]+})', self.script)

        self.raw_info = infos[0] if len(infos) else None
        self.info = eval(self.raw_info) if self.raw_info else {}

        name = self.info.get('_name', script_path.stem.replace('_', ' ').title() )
        icon = self.info.get('_icon')
        '''


        self.dialog = VDialog(title=self.name, Spacing='normal', Padding='big',
        icon=self.icon)

        form_widget = FWidget(self.dialog, Spacing='normal')

        self.widgets = []
        for arg in self.arguments:
            text = arg.name
            if arg.type is bool:
                field = Bool(value=arg.default, name=arg.name)
                text = ''
            elif arg.type is str:
                field = String(value=arg.default)
            elif arg.type is int:
                field = Int(value=arg.default, decimals=0)
            elif arg.type is float:
                field = Float(value=arg.default)
            elif arg.type in (tuple, list):
                field = Enum(value=arg.default, items=arg.items)

            form_widget.layout.row(text,  field)

            self.widgets.append( field )

        ok_btn = PushButton(parent=self.dialog, text='OK', connect=self.valid)
        ok_btn.setAutoDefault(False)

        super().__init__(menu, icon=self.icon, text=self.name, connect=self.dialog.exec_)

    def _get_func_from_path(self, path, func_name):
        from importlib import util

        spec = util.spec_from_file_location(func_name, path)
        mod = util.module_from_spec(spec)

        spec.loader.exec_module(mod)

        return getattr(mod, func_name)


    def valid(self):
        '''
        if self.info:
            info = self.info.copy()
            for k, v in self.widgets.items():
                value = v.value
                if isinstance(self.info[k], (tuple, list)):
                    value = v.items.copy()
                    index = value.index(v.value)
                    value[0], value[index] = value[index], value[0]

                info[k] = value
        '''

        cmd = [sys.executable, str(self.path)]

        for argument, widget in zip( self.arguments, self.widgets):
            if argument.type is bool:
                if widget.value:
                    cmd += [argument.arg]
            else:
                cmd += [argument.arg, widget.value]

        print(cmd)

        subprocess.Popen(cmd)

        #info_replace = str(info)+'\n'*self.raw_info.count('\n')
        #exec_script = self.script.replace(self.raw_info, info_replace, 1)

        #### Test with getting the help message
        #cmd = [sys.executable, str(self.path), '--help']

        #p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #stdout, stderr = p.communicate()
        #help = stdout.decode("utf-8")


        self.dialog.close()
"""


class AssetWidget(HWidget):
    def __init__(self, asset_data):
        super().__init__(tag='AssetList', props={'Hpadding': 'normal'})
        self.label = TextLabel(self, asset_data['name'])

class AssetListWidget(ListWidget):
    value_changed = Signal(str)

    def __init__(self, parent=None, items=[], tag=None, props={}):
        self.props = {'Hmargin': 'normal', 'Vmargin': 'normal'}
        super().__init__(parent, tag='AssetList', props=self.props)

        self.setAlternatingRowColors(True)
        self.setSizeAdjustPolicy(QListWidget.AdjustToContents)

        for i in items:
            self.addItem(i)

        self.currentItemChanged.connect(self.on_item_changed)


    def widgets(self):
        items = [self.items(i) for i in self.count()]
        widgets = [self.itemWidget(i) for i in items]
        return [w for w in widgets if w]

    def addWidget(self, widget, name):
        item = QListWidgetItem(self)
        item.name = name
        item_widget = widget
        item.setSizeHint(widget.sizeHint())
        self.setItemWidget(item, item_widget)
        super().addItem(item)

        if not self.currentItem():
            self.setCurrentItem(item)

    def on_item_changed(self, i, j):
        item = i if i else j
        self.value_changed.emit(item.name)
