

import sys

from canevas.ui.application import QtApplication
from canevas.ui.theme import Themes

from PySide2.QtWidgets import QApplication

import gadget
from gadget.entities import Studio
from gadget.constants import THEMES_DIR, ICONS_DIR

from gadget.ui.settings import Settings
from gadget.ui.signals import Signals
from gadget.ui.worker import Worker
from gadget.ui.context import Context

import os


class Application:
    def __init__(self, studio=None, project=None, ignore_prefs=False):

        self.qt_app = QApplication.instance() or QtApplication()

        self.ignore_prefs = ignore_prefs
        self.studio = Studio.from_config(studio)

        self.studio.set_project(project)

        self.signals = Signals()
        self.settings = Settings(self)
        self.context = Context(self)
        
        os.environ['ICONS_DIR'] = str(ICONS_DIR)
        self.themes = Themes.from_dir(THEMES_DIR)

        self.themes['dark'].apply()

        gadget.app = self
        gadget.signals = self.signals
        gadget.context = self.context
        gadget.settings = self.settings

        sys.modules.update( {
            'gadget.app' : self,
            'gadget.signals' : self.signals,
            'gadget.context' : self.context,
            'gadget.settings' : self.settings
        } )

        self.qt_app.aboutToQuit.connect(self.signals.about_to_quit)

        self.start_workers()
        self.signals.about_to_quit.connect(self.stop_workers)


        from gadget.ui.windows.gadget_window import GadgetWindow
        self.main_window = GadgetWindow()

        self.load_project()



    def start_workers(self):
        self.network_worker = Worker(self)
        self.network_worker.start()

        self.disk_worker = Worker(self)
        self.disk_worker.start()

    def stop_workers(self):
        self.network_worker.stop()
        self.network_worker.wait()

        self.disk_worker.stop()
        self.disk_worker.wait()


    def load_project(self):
        p = self.context.project
        if not p:
            return

        self.signals.project_updated.emit(p)

        w = self.network_worker
        t = p.tracker
        
        url = self.settings.user.url
        login = self.settings.user.login
        password = self.settings.user.password

        w.add_job(t.connect, url, login, password,
        connect=lambda x : setattr(p, 'user_id', x) )
        w.add_job(t.get_project, p.name, connect=p.set_data)
        w.add_job(t.get_persons, connect=p.persons.set)
        #w.add_job(t.get_task_status, connect=p.task_statuses.set)
        #w.add_job(t.get_task_types, connect=p.task_types.set)
        #w.add_job(t.get_entity_types, connect=p.entity_types.set)
        #w.add_job(self.signals.types_setted.emit)
        #w.add_job(t.get_sequences, connect=p.sequences.set)
        #w.add_job(t.get_assets, connect=p.assets.set)
        #w.add_job(t.get_shots, connect=p.shots.set)
        #w.add_job(self.signals.tasks_setted.emit)
        #w.add_job(t.get_comments, connect=p.set_comments)
        #w.add_job(self.signals.project_setted.emit)

    def show(self) :
        self.main_window.show()
        self.qt_app.exec_()