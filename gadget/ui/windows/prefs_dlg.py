
import os
from pathlib import Path

import gadget
#from gadget.widgets.custom_widgets import *
from canevas.ui.base_widgets import HDialog, TabWidget, VWidget, FWidget, PushButton, ScrollArea
#from gadget.widgets.properties import *
from gadget.constants import *
import json


class PrefsDlg(HDialog) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Preferences', windowFlags=['Tool'],
        Background='very_dark')

        #gadget.app.resize(self, 360, 480 )


        self.tabs = TabWidget(self, Padding='normal')

        # General Tab
        self.general_tab = VWidget(title='General', align='Top',
        Background='dark', Padding='normal')

        return

        self.general_tab.form = FWidget(self.general_tab, spacing=5)
        for k, v in gadget.prefs.general.items() :
            v.to_widget(self.general_tab.form, reset=True) #self.general_tab.layout.row(k, v.to_widget() )
        
        self.general_tab.addStretch()
        self.apply_change_btn = PushButton(self.general_tab, text='Apply Change')
        #self.general_tab.addStretch()
        #self.general_tab.layout().addStretch()

        self.project_tab = FWidget(title='Project', align='Top', spacing=5,
        Background='dark', Spacing='normal', Padding='normal')

        #self.project_tab.layout.row(k,  )
        #self.project_tab.addStretch()
        #self.work_in_local_cbox = self.project.prefs['work_in_local'].to_widget(name='Work in local')
        #self.project_tab.row('', self.work_in_local_cbox )

        #self.local_root_ledit = local_root.to_widget()
        #self.project_tab.row('Local Root', self.local_root_ledit )

        #self.projects_uilist = self.prefs.general['projects_conf'].to_widget()
        #self.general_tab.row('Projects', self.projects_uilist )

        #self.envs_uilist = self.prefs.general['environments'].to_widget()
        #self.general_tab.row('Environments', self.envs_uilist )

        # Env Tab
        self.env_tab = FWidget(title='Envs', align='Top', spacing=5,
        Background='dark', Spacing='normal', Padding='normal')

        #self.env_tab.addStretch()

        # Display Tab
        self.display_tab = FWidget( title='Display', align='Top', spacing=5,
        Background='dark', Spacing='normal', Padding='normal')
        for k, v in gadget.app.prefs.display.items() :
            v.to_widget(self.display_tab)#self.display_tab.row(k, v.to_widget() )

        for t in ( self.general_tab, self.project_tab, self.env_tab, self.display_tab ) :
            self.tabs.addTab( ScrollArea(widget=t), t.windowTitle() )

        gadget.signals.project_updated.connect(self.set_project_tab)
        gadget.signals.project_updated.connect(self.set_env_tab)
        #self.addStretch()
        #self.restore_project_settings()
    def set_project_tab(self):
        self.project_tab.layout().clear()
        for k, v in gadget.context.project.prefs.items() :
            v.to_widget(self.project_tab)

    def set_env_tab(self):
        self.env_tab.layout().clear()
        '''
        for k, v in gadget.context.project.env_props.items() :
            v.to_widget(self.set_env_tab)#self.display_tab.row(k, v.to_widget() )
        '''


        '''
    def save_settings(self):
        APP.settings.setValue('ui_scale', APP.ui_scale )
        APP.settings.setValue('projects', self.projects_ui_list.to_list() )
        APP.settings.setValue('envs', self.envs_ui_list.to_list() )

    def restore_settings(self) :
        ui_scale = APP.settings.value('ui_scale', None)
        if ui_scale is not None :
            APP.preferences.display['ui_scale'].value = float(ui_scale)

        ## Projects conf list
        projects = APP.settings.value('projects', [])
        if not projects or not any(projects):
            projects= []#CONFIG['projects']

        if not isinstance(projects, (list, tuple)) :
            projects = [projects]

        for p in projects :
            if not p : continue
            self.projects_ui_list.add( str(p) )

        ## Envs conf list
        envs = APP.settings.value('envs', [])
        if not isinstance(envs, (list, tuple)) :
            envs = [envs]

        for e in envs :
            if not e : continue
            self.envs_ui_list.add( str(e) )


    def save_project_settings(self):
        if not APP.project : return
        local_root = Path(self.local_root.value).as_posix()
        APP.project.settings.setValue('local_root', local_root )
        APP.settings.setValue('work_in_local', self.work_in_local.value )

    def restore_project_settings(self) :
        if not APP.project : return

        local_root = APP.project.settings.value('local_root', None)
        if local_root is not None:
            self.local_root.value = local_root
            #value = Path(value).as_posix()
            #APP.project.env['LOCAL_ROOT'] = value
        work_in_local = APP.settings.value('work_in_local', None)
        if work_in_local is not None :
            if isinstance(work_in_local, str) :
                work_in_local = json.loads(work_in_local)
            self.work_in_local.value = work_in_local
            '''
