
import os

from canevas.ui.widgets import VWidget, HWidget, PushButton, TextLabel, \
Frame, LineEdit, VListWidget, IconButton, ListEnum, StringLineEdit, \
Splitter, FWidget, Menu, MenuButton, GridListWidget, Panel, StackedWidget, \
WidgetButton

from canevas.ui.property_group import PropertyGroup
from canevas.ui.properties import EnumProperty, BoolProperty, StringProperty

#from gadget.ui.entities.soft_widget import SoftWidget
from gadget.ui.widgets import EnvWidget
from gadget import signals, context


class SoftWidget(HWidget):
    def __init__(self, soft, parent=None):
        super().__init__(parent=parent, spacing=5, margin=8)

        #print('### SOFT', soft, soft.name)

        #print([v.name for v in soft.versions])

        self.properties = PropertyGroup(
            version=EnumProperty(name='Version', items=[v.name for v in soft.versions])
        )

        self.soft = soft
        self.name = soft.name

        #self.setFixedWidth(215)

        print('\n#DEBUG SoftWidget', soft)
        print([v for v in soft.versions])

        self.launch_btn = PushButton(parent=self, icon=soft.icon,
        size=(64, 64), iconSize=48, connect=self.on_launch_clicked )

        self.version_widget = VWidget(parent=self)

        label = TextLabel(parent=self.version_widget, text=soft.name, FontWeight='normal')
        self.version_widget.addStretch()
        self.version_row = HWidget(parent=self.version_widget)
        self.versions_cbbox = self.properties['version'].to_widget(self.version_row, sizePolicy=('Expanding', 'Fixed'))
  
        self.properties['version'].value_changed.connect(self.set_active_version)
        self.set_active_version(self.properties.version)
        #self.restore_settings()
        #APP.signals.closed.connect(self.save_settings)


    def set_active_version(self, version):
        #print('\n6540', self.soft.prefs['version'].exclusive, self.soft.prefs['version'].value, version)
        soft_name = self.soft.name.upper().replace(' ', '_')

        context.project.env[soft_name+'_VERSION'] = version

    def on_launch_clicked(self, modifiers):
        #version = self.properties.version
        print('Launch Version', self.properties.version)
        self.soft.versions[self.properties.version].launch()


    '''
    def restore_settings(self):
        soft_version = APP.project.settings.value(self.soft.name, None)

        if soft_version:
            self.versions_enum.value = soft_version


    def save_settings(self):
        APP.project.settings.setValue(self.soft.name, self.versions_enum.value )
    '''


class SoftFilterMenu(Menu):
    def __init__(self, softs_window):
        super().__init__(softs_window)

        self.softs_window = softs_window
        self.form = FWidget(self, spacing=5, margin=5)

        sort_items = ['Name']

        self.properties = PropertyGroup(
            sorted_by = EnumProperty(parent=self.form, name='Sorted', items=sort_items),
            show_hidden=BoolProperty(parent=self.form, name='Show Hidden'),
        )

        self.properties.value_changed.connect(lambda: softs_window.filter() )
        self.properties.value_changed.connect(lambda: softs_window.sort() )


class SoftsWindow(VWidget):
    def __init__(self, parent=None):
        super().__init__(parent, title='Softs', margin=0, spacing=0, Background='dark')

        #self.ui_setted = False

        signals.closed.connect(self.save)
        #signals.project_updated.connect(self.set_ui)
        self.properties = PropertyGroup(
            soft_state=EnumProperty(items=['Installed', 'All']),
            search=StringProperty()
        )

        self.search_bar = HWidget(self, collapse=True, margin=8, 
        sizePolicy=('Expanding', 'Fixed'))

        self.properties['soft_state'].to_widget(self.search_bar, mode='expand', 
        sizePolicy='Fixed', connect=lambda: self.filter())

        self.properties['search'].to_widget(self.search_bar, icon='search',
        clear=True, connect=self.filter)

        self.filter_menu = SoftFilterMenu(self)
        self.filter_btn = MenuButton(self.search_bar, icon='filter', menu=self.filter_menu)
        

        Frame(self.search_bar, size=(10, None))

        self.install_btn = IconButton(self.search_bar, icon='install',
        connect=self.install_softs, toolTip='Install Selected Softs')

        self.uninstall_btn = IconButton(self.search_bar, icon='bin', 
        connect=self.uninstall_softs, toolTip='Remove Selected Softs')

        #Frame(self.search_bar, size=(10, None))
        self.refresh_btn = IconButton(self.search_bar, icon='refresh', size=(24, None),
        tooltip='Refresh Soft List')
        
        Frame(self, size=(None, 1), Background='very_dark')


        self.splitter = Splitter(self, 'Horizontal')

        self.list_wgt = GridListWidget(self.splitter, itemSize=(200, 80),
        tag='EntityList', selectionMode='ExtendedSelection')
        self.list_wgt.sortKey = self.sortKey



        #self.install_btn = IconButton(parent=self.splitter, icon='install', Emboss='none',
        #connect=self.install_softs)
        self.list_wgt.currentRowChanged.connect(self.set_settings_widget)

        self.properties_frame = StackedWidget(self.splitter, Background='medium_dark')

                
        self.set_softs()   
        #self.ui_setted = True    

        #self.restore() 

    def set_settings_widget(self, item_index):
        item = self.list_wgt.item(item_index)
        self.properties_frame.setCurrentWidget(item.settings_widget)

        print(item.widget().soft.env)
 

    def sort(self):
        self.list_wgt.sortItems()

    def sortKey(self, item):
        sorting = self.filter_menu.settings.sorted_by

        key = None

        if not item.widget():
            key = item.text()
        elif sorting == 'Name':
            key = item.widget().entity.name

        return item.text() if key is None else str(key)

    def filter(self, search=None, soft_widgets=[]):
        return

    def install_softs(self):
        ## Add to the worker thead
        for soft_widget in self.list_wgt.selectedWidgets():
            version = soft_widget.soft.versions.active
            gadget.app.disk_worker.add_job(version.install)

    def uninstall_softs(self):
        ## Add to the worker thead
        for soft_widget in self.list_wgt.selectedWidgets():
            version = soft_widget.soft.versions.active
            gadget.app.disk_worker.add_job(version.uninstall)

    def filter(self):
        search = self.search_ledit.text().lower()
        for w in self.list_wgt.widgets():
            soft_name = w.soft.name.lower()
            w.item.setHidden(search not in soft_name)

    def set_soft(self, soft):
        soft_widget = SoftWidget(soft)
        item = self.list_wgt.addItem(text=soft.name, widget=soft_widget)

        item.settings_widget = VWidget(self.properties_frame)
        env_panel = Panel(item.settings_widget, title='Environment')
        env_widget = EnvWidget(env_panel.body, soft.env)

        
        
        #self.properties_frame.addWidget()

    def set_softs(self, softs=None):
        if not softs:
            softs = context.project.softs

        for soft in softs:
            if soft.name == 'default' or soft.hide: continue
            self.set_soft(soft)

    def save(self):
        if not self.ui_setted:
            return

        data = {
            'selected': [i.text() for i in self.list_wgt.selectedItems()],
            'current': self.list_wgt.currentText(),
            'state': self.state.value()
        }
        QSettings().setValue('windows/'+self.windowTitle(), data )
    
    def restore(self):
        if gadget.app.ignore_prefs:
            return

        lw = self.list_wgt

        try:
            data = QSettings().value('windows/'+self.windowTitle())    
            self.state.setValue(data['state'])
            print('\nRestore', self.windowTitle(), data)
            for i in lw.items():
                print(i.text())
                i.setSelected(i.text() in data['selected'])
                if i.text() == data['current']:
                    lw.setCurrentItem(i)
                    
        except Exception as e:
            print('Failed to restore value for window', self.windowTitle())
            print(e)