
#from gadget.widgets.properties import Enum
#from PySide2.QtCore import QSettings

import gadget

#from gadget import PropertyGroup
#from gadget.base_objects.properties import EnumProperty
from canevas.ui.widgets import HWidget, VWidget, PushButton, \
Menu, TabWidget, Splitter, ProgressBar, IconButton, Icon, TextLabel, \
Console, WidgetIcon, Frame

#from gadget.widgets.custom_widgets import Console
from gadget.ui.windows.prefs_dlg import PrefsDlg #SoftsWgt, MyTaskWgt, EntityBrowserWgt, LoginDlg, PrefsDlg
from gadget.ui.windows.softs_window import SoftsWindow
from gadget.ui.windows.my_task_window import MyTaskWindow
from gadget.ui.windows.login_dlg import LoginDlg

from gadget import app, signals, studio, settings, context


class GadgetWindow(VWidget) :
    def __init__(self):
        super().__init__(title='Gadget', Background='very_dark')

        Frame(self, Background='very_dark', size=(None, 1))
        self.top_bar = HWidget(self, margin=(8,5,5,5), spacing=5, Background='medium_dark')


        self.project_icon = WidgetIcon(self.top_bar, size=(25, None), iconSize=20)
        self.set_project_icon()

        self.project = TextLabel(self.top_bar, text=context.project.name, FontSize=17, FontWeight='light', FontColor='disabled')
        #settings.general['projects'].to_widget(parent=self.top_bar, size=(None, 32),
        #FontSize=14, FontWeight='normal')
        
        #PushButton(self.top_bar, 'My Project')
        if context.project.type == "TV Show":
            self.arrow_icon = WidgetIcon(self.top_bar, icon='arrow_right', size=(32, None))
            #self.episode_row = HWidget(self.top_bar, collapse=True)
            #self.episode_letter = TextLabel(self.episode_row, text='E', Background='highlight', Padding=6)
            self.episode = settings.general['episodes'].to_widget(parent=self.top_bar, FontSize=14, FontWeight='normal')

        self.project_row = HWidget(self.top_bar, spacing=5)

        self.top_bar.addStretch()

        self.buttons_row = HWidget(self.top_bar, spacing=0, collapse=True)

        self.btn_refresh = IconButton(self.buttons_row, icon='refresh', iconSize=20, Emboss='none',
        toolTip ='Refresh', connect=app.load_project )

        ## Btn kitsu Connect
        self.user_btn = IconButton(self.buttons_row, icon='settings', iconSize=20, Emboss='none',
        toolTip ='Login to tracker', connect=lambda : PrefsDlg(self).show())

        #connect= PrefsDlg(self).show )
        ## Btn Settings
        #self.prefs_window = PrefsWindow(self)
        self.prefs_btn = IconButton(self.buttons_row, icon='user', iconSize=20, Emboss='none',
        toolTip ='Preferences', connect=lambda : LoginDlg(self).exec_())

        self.body = Splitter(self, 'Vertical')


        self.tabs = TabWidget(self.body)

        self.softs_tab = SoftsWindow()
        self.my_task_tab = MyTaskWindow()
        #self.entity_browser_tab = EntityBrowserWgt()

        #tabs = (self.softs_tab, self.my_task_tab, self.entity_browser_tab)
        tabs = (self.softs_tab, self.my_task_tab)
        for t in tabs :
            self.tabs.setTabWidget(t.windowTitle(), t)
        

        self.progress_bar = ProgressBar(self)
        signals.progress_updated.connect(self.update_progress_bar)

        # Console
        #self.console = Console(self.body)

        self.body.setSizes([self.body.height() * 0.99,
                            self.body.height() * 0.01])
        self.body.setStretchFactor(0, 1)
        self.body.setStretchFactor(2, 1)
        '''

        app.signals.closed.connect(self.save)
        self.restore()
        #self.body.addWidget(self.tabs)
        #self.body.addWidget(self.console)

        #self.resize(*APP.resize(560, 680))
        '''
        self.resize(560, 680)

    def set_project_icon(self):
        icon = 'movie'

        if context.project.icon:
            icon = context.project.icon

        elif context.project.type == "TV Show":
            icon = 'tv'
        elif context.project.type == "Short":
            icon = 'video'

        self.project_icon.setIcon(Icon(icon))

    def update_progress_bar(self) :
        self.progress_bar.setValue(context.progress)
        self.progress_bar.update()

    def set_ui(self):
        self.project_row.clear()

        project = self.studio.context.project
        if project.type == "TV Show":
            self.episode = app.prefs.general['episodes'].to_widget(parent=self.top_bar)


