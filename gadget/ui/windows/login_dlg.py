
#from PySide2.QtCore import Qt
#from PySide2.QtGui import *
#from PySide2.QtWidgets import QApplication, QLineEdit, QLayout

#from gadget.widgets.properties import Enum, String
from canevas.ui.base_widgets import PushButton, VDialog, FormLayout
#from gadget.constants import *
#from gadget.utils import *

import gazu
import os

import gadget
from gadget import settings


class LoginDlg(VDialog) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Login', margin=10, spacing=8)

        self.resize(350, 0)

        self.form_lyt = FormLayout(self, spacing=6)
        #self.form_lyt.setSizeConstraint(QLayout.SetNoConstraint)

        self.url = settings.user['url'].to_widget(self.form_lyt, reset=True, icon='internet')
        self.login = settings.user['login'].to_widget(self.form_lyt, reset=True, icon='user_fill')
        self.password = settings.user['password'].to_widget(self.form_lyt, reset=True, echo_mode='Password', icon='lock')
        self.connect = PushButton(self, text='Connect')
        self.connect.clicked.connect(self.valid)

    def valid(self) :
        gadget.context.project.set()