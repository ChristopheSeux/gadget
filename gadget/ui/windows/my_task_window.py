
from PySide2.QtCore import QCoreApplication, Qt, Signal, QSettings
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QAbstractItemView, QSizePolicy, QMenu, QWidgetAction

from pathlib import Path

from canevas.ui.properties import *
from canevas.ui.property_group import PropertyGroup
from canevas.ui.widgets import HWidget, VWidget, Frame, TextLabel, ComboBox,\
PushButton, Icon, LineEdit, VListWidget, Menu, FWidget, HLayout, VDialog, \
IconButton, ListEnum, StringLineEdit, MenuButton, WidgetButton

from gadget.constants import *

import os
import tpl
import gadget

from gadget import app, settings, signals, context

'''
class OpenTaskDlg(VDialog):
    def __init__(self, version, local_version, parent=None, connect=None):
        super().__init__(parent, title='Warning', Background='dark',
        Padding='normal', Spacing='normal')

        APP = QApplication.instance()
        self.resize(*APP.resize(320, 5) )
        self.task = version.task
        self.version = version
        self.local_version = version

        self.label = TextLabel(self, text=f'A newer version {version.name} is available on the network:')
        self.button_group = QButtonGroup()

        if local_version:
            cb = CheckBox(self, text=f'Open local version {local_version.name}')
            cb.action = 'open'
        else:
            cb = CheckBox(self, text='Build version and open it')
            cb.action = 'build'

        self.button_group.addButton(cb)

        cb = CheckBox(self, text=f'Copy network version {version.name} and open it')
        cb.action = 'synchronise'
        self.button_group.addButton(cb)

        cb.setChecked(True)

        if connect:
            self.valided.connect(connect)

        self.ok = PushButton(self, text='OK', connect=self.valid)

    def valid(self):
        action = self.button_group.checkedButton()
        if action == 'open':
            self.local_version.open()
        elif action == 'build':
            version = self.task.build()
            version.open()
        elif action == 'synchronise':
            version = self.version.synchronise()
            version.open()

        self.close()
'''

class CommentDlg(VDialog) :
    def __init__(self, parent=None, connect=None):
        super().__init__(parent, title='Publish', spacing=8, margin=8)

        self.resize(240, 0)

        form_widget = FWidget(self, spacing=8)

        self.status = EnumProperty(parent=form_widget, focus='NoFocus',
        items=['TODO','WIP', 'WFA', 'DONE'], expand=True, name='Status')
        #self.status.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        #form_widget.layout.row('Status :', self.status)

        self.comment = TextProperty(parent=form_widget, name='Comment')
        #form_widget.layout.row('Comment :', self.comment)

        #self.playblast = BoolProperty(parent=form_widget, name='Make Playblast:', focus='NoFocus')
        #self.playblast.setFocusPolicy( Qt.NoFocus )
        #form_widget.layout.row('Make Playblast:', self.playblast)


        self.ok = PushButton(self, text= 'OK')

        if connect :
            self.ok.clicked.connect(connect)

        self.ok.clicked.connect(self.close)


class TaskLabel(HWidget):
    def __init__(self, parent, task):
        super().__init__(parent, align='Left')

        self.frame = HWidget(self, align='Left', Height='normal', Background='normal')
        self.frame.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.task_color = Frame(self.frame , Width='micro', Height='normal')

        #c = task.task_type.color
        #color = ( c.red(), c.green(), c.blue() )
        self.task_color.setStyleSheet(f"background-color: {task.task_type.color.name()};")
        #print(task.task_type.color, f"background-color: {task.task_type.color};")
        Frame(self.frame , Size='micro')
        self.task_lbl = TextLabel(self.frame , task.name, FontWeight='normal')
        Frame(self.frame , Size='micro')

        self.setMinimumWidth(75)

        #Frame(self, Size='micro')

        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)




class TaskWidget(HWidget):
    def __init__(self, task):
        super().__init__(spacing=5, margin=(1, 1, 8, 2), align=('Left','Center') )

        self.version = EnumProperty(name='Version')
        self.is_opened = False

        self.setAcceptDrops(True)
        self.task = task
        self.entity = self.task.entity

        self.thumbnail_lbl = Thumbnail(self, size=(72, 48))


        self.task_label = TaskLabel(self, self.task)
        self.entity_lbl = TextLabel(self, self.entity.label)

        self.addStretch()
        self.status_row = HWidget(self)
        self.status_btn = PushButton(self.status_row, tag='Status', FontSize='small')
        self.set_status()
        task.status_updated.connect(self.set_status)

        Frame(self.status_row, tag='tiny')
        # Priority:
        for i in range(task.priority):
            p = TextLabel(self.status_row, '!', FontSize='Bold')
            p.setStyleSheet("color: red" )
            p.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

        self.last_comment = TextLabel(self, wordWrap=True, FontSize='small', Height=None)
        self.last_comment.setFixedWidth(100)

        self.version_row = HWidget(self)

        self.version_enum = self.version.to_widget(self.version_row, IconSize='tiny')
        self.version_enum.setFocusPolicy(Qt.NoFocus)
        self.version_enum.setMinimumWidth(65)
        self.set_versions()
        self.version.value_changed.connect(lambda x: self.set_screenshot() )
        task.versions_updated.connect(self.set_versions )

        self.open = PushButton(self.version_row, icon='open_32',
         toolTip='Open Task',
        connect=self.on_open_clicked )

        task.comments_updated.connect(self.set_last_comment)

    def set_last_comment(self, comments):
        comment = comments[-1].text if comments else ''

        QCoreApplication.processEvents()
        self.last_comment.setText(comment)

    def get_version(self):
        version_name = self.version.value
        if version_name and version_name != 'None':
            return self.task.versions[version_name]

    def set_status(self):
        status = self.task.status
        c = status.color.darker(135)
        self.status_btn.setStyleSheet(f'background: rgb{c.red(), c.green(), c.blue()}; color: rgb(240,240,240)')
        self.status_btn.setText(status.norm_name)

    def set_screenshot(self, screenshot=None):
        if screenshot:
            self.thumbnail_lbl.set_pixmap(screenshot)
        else:
            #version = self.get_version()
            #pixmap = version.screenshot if version else None
            self.thumbnail_lbl.set_pixmap(self.task.screenshot)

    def set_versions(self):
        #print('Set Version', print(self.versions), len(self.versions))
        self.version.clear()

        if not self.task.versions:
            self.version.add_item(text='None')
            #self.version.add_item('', 'Toto')
            #print(self.task.name)
            return

        for v in self.task.versions:
            if v.on_local and v.on_server:
                icon = 'on_local_and_server_16'
            elif v.on_local:
                icon = 'on_local_16'
            elif v.on_server:
                icon = 'on_server_16'
            else:
                icon = ''

            self.version.add_item(Icon(icon), v.name)

        if self.version.items:
            self.version.value = self.version.items[-1]

    def dragEnterEvent(self, e):
        list_wgt = self.item.listWidget()

        if e.mimeData().hasText():
            e.accept()

            list_wgt.setCurrentItem(self.item)

            for i in range(list_wgt.count() ):
                item = list_wgt.item(i)
                item.setSelected(item is self.item)

        else:
            e.ignore()
            print ("ignore")

    def dropEvent(self, e):
        print ("drop", e.mimeData().text())

        files = [Path(u.toLocalFile() ) for u in e.mimeData().urls()]
        file = Path(files[0])

        if file.suffix == '.blend':
            self.task.insert_file(file)

        elif file.suffix == '.png':
            self.entity.set_preview(file)

        elif file.is_dir():
            print('Dir')

    def on_open_clicked(self, modifiers):
        version = self.get_version()

        # Open the folder
        if modifiers == Qt.ControlModifier and version:
            return open_file(version.path.parent)

        if not version:
            self.task.build(background=False)
            #self.set_versions()
            return

        if version.work_in_local:
            if not version.local_path.exists():
                version.download()
            version.open()
        else:
            version.open()


class TaskFilterMenu(Menu):

    def __init__(self, my_task_wgt):
        super().__init__(my_task_wgt)

        self.my_task_wgt = my_task_wgt
        self.form = FWidget(self, spacing=5, margin=5)

        sort_items = ['Name', 'Status', 'Priority', 'Order']
        priority_items = ['Normal', 'High', 'Very High', 'Emergency']
        for_entities = ['Shot', 'Asset']

        self.sorted_by = EnumProperty(parent=self.form, name='Sorted', items=sort_items, 
        connect=lambda : my_task_wgt.sort_updated.emit() )

        self.properties = PropertyGroup(
            statuses=EnumProperty(parent=self.form, name='Status', exclusive=False, mode='expand'),
            for_entities=EnumProperty(parent=self.form, name='For Entities', exclusive=False, 
                mode='expand', items=for_entities, value=for_entities),
            shot_tasks=EnumProperty(parent=self.form, name='Shot Tasks', exclusive=False, mode='expand'),
            asset_tasks=EnumProperty(parent=self.form, name='Asset Tasks', exclusive=False, mode='expand'),
            asset_types=EnumProperty(parent=self.form, name='Asset Types', exclusive=False, mode='expand'),
            priorities=EnumProperty(parent=self.form, name='Priorities', exclusive=False, mode='expand', 
                items=priority_items, value=priority_items),
        )

        settings.general['statuses'].items_changed.connect(self.set_statuses)
        settings.general['shot_tasks'].items_changed.connect(self.properties['shot_tasks'].set_items)
        settings.general['asset_tasks'].items_changed.connect(self.properties['asset_tasks'].set_items)
        settings.general['asset_types'].items_changed.connect(self.properties['asset_types'].set_items)

        #self.restore()

        self.properties.value_changed.connect(lambda : my_task_wgt.filter_updated.emit() )
        #self.props['priorities'].widgets[0].valueChanged.connect(self.debug )
        #dlp.app.signals.project_setted.connect(self.restore)
        #dlp.app.signals.closed.connect(self.save)

    def set_statuses(self, items):
        p = context.project
        items = [i for i in items if not p.task_statuses[i].is_done]
        self.properties['statuses'].set_items(items)

    def save(self):
        self.props.save('windows/my_task/filter')
        #data = self.props.to_dict()
        #QSettings().setValue('windows/my_task/filter', data )

        #print('\n11 Save filter', self.props.to_dict())
    
    def restore(self):
        #print('\n22 Restore filter', QSettings().value('windows/my_task/filter') )
        self.props.restore('windows/my_task/filter')
        #data = QSettings().value('windows/my_task/filter')
        #self.props.restore(data)

        #print('restore filter', data)


class MyTaskWindow(VWidget):
    filter_updated = Signal()
    sort_updated = Signal()
    #task_updated = Signal()

    def __init__(self, parent=None):
        super().__init__(parent, title='My Task', margin=0, spacing=0, align='Top',
        Background='dark')

        #self.ui_setted = False

        #signals.project_updated.connect(self.set_ui)
        signals.tasks_updated.connect(self.add_tasks)
        signals.types_setted.connect(self.restore)
        signals.tasks_setted.connect(self.restore)
        signals.closed.connect(self.save)

        self.filter_updated.connect(self.filter )
        self.sort_updated.connect(self.sort)

        self.properties = PropertyGroup(
            task_state=EnumProperty(items=['Task to do', 'Done']),
            search=StringProperty()
        )

        self.search_bar = HWidget(self, collapse=True, margin=8, 
        sizePolicy=('Expanding', 'Fixed'))

        self.properties['task_state'].to_widget(self.search_bar, mode='expand', 
        sizePolicy='Fixed', connect=lambda: self.filter())

        self.properties['search'].to_widget(self.search_bar, icon='search',
        clear=True, connect=self.filter)

        self.filter_menu = TaskFilterMenu(self)
        self.filter_btn = MenuButton(self.search_bar, icon='filter', menu=self.filter_menu)

        Frame(self.search_bar, size=(10, None))

        self.comment_dlg = CommentDlg(self, connect=self.on_comment_clicked)
        self.comment_btn = IconButton(self.search_bar, icon='comment',
        toolTip='Comment Selected Tasks', connect=self.comment_dlg.show )
        
        #Frame(self.search_bar, tag='small')

        #Frame(self.search_bar, size=(10, None))
        self.refresh_btn = IconButton(self.search_bar, icon='refresh', size=(24, None),
        tooltip='Refresh My Tasks')

        #Frame(self.search_bar, tag='normal')
        self.actions_layout = HLayout(self.search_bar)
        
        '''
        Frame(self.search_bar, tag='normal')
        self.download_btn = PushButton(self.search_bar, icon='import_32',
        toolTip='Download Selected Versions to Local',
        connect = self.on_download_clicked )
        
        self.upload_btn = PushButton(self.search_bar, icon='export_32',
        toolTip='Upload Selected Versions to Server',
        connect = self.on_upload_clicked )

        #self.set_actions()

        
        Frame(self.search_bar, tag='normal')
        self.publish = PushButton(self.search_bar, icon='playblast_32',
         toolTip='Publish Selected Tasks',
        connect = lambda: PublishDlg(self, connect=self.on_publish_clicked).show() )

        self.publish = PushButton(self.search_bar, icon='play_32',
         toolTip='Publish Selected Tasks',
        connect = lambda: PublishDlg(self, connect=self.on_publish_clicked).show() )

        Frame(self.search_bar, tag='normal')
        self.action_btn = PushButton(self.search_bar, icon='menu_32', tag='Menu',
         toolTip='Open Action Menu' ) 
        self.action_btn.setMenu( TaskFilterMenu(self) )
        '''

        #Frame(self.search_bar, tag='small')
        #APP.kitsu.my_tasks_ready.connect(self.set_ui)
        self.list_wgt = VListWidget(self, tag='AssetList', selectionMode='ExtendedSelection')
        #self.list_wgt.setSelectionMode(QAbstractItemView.ExtendedSelection)
        # Signals

        # Have to clear when project changed
        
        
        #dlp.app.signals.comments_updated.connect(self.set_last_comments)
        #dlp.app.signals.shot_tasks_updated.connect(self.add_tasks)
        #APP.signals.connection_updated.connect(self.on_connection_update)
        #self.ui_setted = True

        #self.list_wgt.sortKey = self.sortKey

        #self.set_actions()
        #self.restore()


    def set_actions(self):
        #return

        #print('\n >>>>>>> Set My Tasks actions')
        self.actions_layout.clear()
        for interface in context.project.interfaces:
            print(interface.locations)
            #print('\n>>>>>>>>>>> Action', action, action.locations)
            if 'main_window/my_task/tasks' in interface.locations:
                interface.to_widget(self.actions_layout)
            pass

    def get_tasks(self):
        return [ w.task for w in self.list_wgt.widgets() ]

    def get_selected_tasks(self):
        return [ w.task for w in self.list_wgt.selectedWidgets() ]

    def get_selected_versions(self):
        return [ w.get_version() for w in self.list_wgt.selectedWidgets() ]

    def set_connection(self, connection):
        for w in self.list_wgt.widgets():
            print(w)

    def on_connection_update(self, connection):
        icon = QIcon()
        #path = connection.filepath

        entity = connection.entity
        if entity:
            for task in entity.tasks:
                task.set_versions()

    '''
    def on_upload_clicked(self):
        for v in self.get_selected_versions():
            dlp.app.disk_worker.add_job(v.upload)
            #version.upload() # Need to Add to Worker
 
    def on_download_clicked(self):
        for v in self.get_selected_versions():
            dlp.app.disk_worker.add_job(v.download)
            #version.download() # Need to Add to Worker
    
    
    def on_play_clicked(self):
        outputs = []
        for t in self.get_selected_tasks():
            output =  t.tpl_output.find(t.format_data,
            version=template.LAST, ext='mov')

            if output:
                outputs.append(output)

        dlp.context.project.shots.play(shots=outputs)

    def on_playblast_clicked(self):
        #print(versions)
        dlp.context.project.shots.playblast(
            versions = self.get_selected_versions(),
            update_libraries = self.playblast_dlg.update_libraries.value,
            burn_stamps = self.playblast_dlg.burn_stamps.value,
            add_to_kitsu = self.playblast_dlg.add_to_kitsu.value,
            play_on_finish = self.playblast_dlg.play_on_finish.value
        )

    def on_build_clicked(self):
        for t in self.get_selected_tasks():
            t.build(background=True, local=False) # Build on Network
            t.set_versions()
    '''
    def on_comment_clicked(self):
        #kitsu = APP.kitsu

        status = self.comment_dlg.status.value
        comment = self.comment_dlg.comment.value

        for version in self.get_selected_versions():
            version.publish(status=status, comment=comment)
            version.task.update()
            #w.set_status()

    def sort(self) :
        self.list_wgt.sortItems()

    def sortKey(self, item):
        sorting = self.filter_menu.sorted_by.value

        key = None

        if not item.widget() :
            key = item.text()
        elif sorting == 'Name':
            key = item.widget().entity.label
        elif sorting == 'Order':
            key = item.widget().entity.data['data'].get('edit_start', 
            item.widget().entity.label )
        elif sorting == 'Priority':
            key = item.widget().task.priority
        elif sorting == 'Status':
            key = item.widget().task.status.norm_name

        return item.text() if key is None else str(key)

    def filter(self, search=None, task_widgets=[]):
        #return
        filters = self.filter_btn.menu().props
        task_state = self.task_state.value()

        #print('FILTER', filters.to_dict())

        #statuses = dlp.context.project.task_statuses

        if not search:
            search = self.search_ledit.text()

        search = search.lower()

        if not task_widgets:
            task_widgets = self.list_wgt.widgets()

        #print('task_widgets', task_widgets)

        for w in task_widgets:
            if not w :
                continue

            task = w.task
            status = task.status #.short_name.upper()
            entity_type = task.entity.type.name
            priority = {0:'Normal', 1:'High', 2:'Very High', 3:'Emergency'}.get(task.priority)
            for_entities = filters.for_entities or []
            priorities = filters.priorities or []

            if search not in task.entity.name.lower():
                w.item.setHidden(True)
                continue
            if task_state == 'Task to do' and status.is_done:
                w.item.setHidden(True)
                continue
            if task_state == 'Done' and not status.is_done:
                w.item.setHidden(True)
                continue
            if task_state != 'Done' and status.norm_name not in filters.statuses:
                w.item.setHidden(True)
                continue
            if task.task_type.name not in filters.shot_tasks+filters.asset_tasks:
                w.item.setHidden(True)
                continue
            if not task.for_shots and entity_type not in filters.asset_types:
                w.item.setHidden(True)
                continue
            if task.for_shots and 'Shot' not in for_entities:
                w.item.setHidden(True)
                continue
            if not task.for_shots and 'Asset' not in for_entities:
                w.item.setHidden(True)
                continue
            if priority not in priorities:
                w.item.setHidden(True)
                continue

            w.item.setHidden(False)

    def add_task_widget(self, task):
        #print('add_task_widget', len(task.versions), task.screenshot)
        QCoreApplication.processEvents()
        task_widget = TaskWidget(task)

        #QCoreApplication.processEvents()
        task_widget.set_screenshot(task.screenshot)
        
        item = self.list_wgt.getItem(task.id)#addWidget(task_widget, task.name)
        item.setWidget(task_widget)
        #item.setWidget(task_widget)

        self.filter(task_widgets=[task_widget])
        self.sort()

        
        self.list_wgt.scrollToItem( self.list_wgt.currentItem(), QAbstractItemView.PositionAtCenter )
        #self.task_updated.emit()

        #QCoreApplication.processEvents()

    def add_task(self, task):
        app.disk_worker.add_job(task.set_versions )
        #dlp.app.disk_worker.add_job(self.add_task_widget, task )
        app.disk_worker.add_job(task.set_screenshot, connect=lambda: self.add_task_widget(task) )

    def add_tasks(self, tasks):
        #print('Set My Tasks', len(tasks))
        #self.list_wgt.clear()

        #P = dlp.context.project
        user = context.user
        tasks = [t for t in tasks if user in t.assignees]
        #if not dlp.context.project or not user: return

        for task in tasks:
            item = self.list_wgt.addItem(task.id)
            item.setHidden(True)
            self.add_task(task)
        #    #if task not in tasks:

    def save(self):
        if not self.ui_setted :
            return

        self.filter_menu.save()

        data = {
            'selected': [i.text() for i in self.list_wgt.selectedItems()],
            'current': self.list_wgt.currentText(),
            'task_state': self.task_state.value(),
            'sorted_by' : self.filter_menu.sorted_by.value
        }
        #print('\Saving MyTask', data)
        QSettings().setValue('windows/'+self.windowTitle(), data )
    
    def restore(self):
        if not self.ui_setted or app.ignore_prefs :
            return

        lw = self.list_wgt

        try:
            data = QSettings().value('windows/'+ self.windowTitle())    
            self.task_state.setValue(data['task_state'])

            print('\nRestore MyTask', data)
            for i in lw.items():
                #print(i.text())
                i.setSelected(i.text() in data['selected'])

                if i.text() == data['current']:
                    lw.setCurrentItem(i)
            
            self.filter_menu.sorted_by.value = data['sorted_by']
            
            self.filter_menu.restore()

        except Exception as e:
            print('Failed to restore value for window', self.windowTitle())
            print(e)
