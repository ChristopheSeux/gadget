
from canevas.ui.properties import *
from canevas.ui.property_group import PropertyGroup

from gadget.constants import *

import json
import os


class Settings:
    def __init__(self, app):
        super().__init__()

        self.app = app
        #self.settings = app.settings

        studio_data = app.studio.data
        project_data = app.studio.project.data
        #self.interface = PropertyGroup()


        self.general = PropertyGroup(
            episodes = EnumProperty(name='Episode', items=project_data['episodes'], exclusive=False),
            statuses = EnumProperty(name='Statuses', exclusive=False),
            asset_types = EnumProperty(name='Asset Type', exclusive=False),
            entity_types = EnumProperty(name='Entity Type', exclusive=False),
            shot_tasks = EnumProperty(name='Shot Task', exclusive=False),
            asset_tasks = EnumProperty(name='Asset Task', exclusive=False),
            project_list = ListProperty(studio_data['projects'], name='Projects studio_data'),
            env_list = ListProperty(studio_data['environments'], name='Environments'),
        )
        self.user = PropertyGroup(
            url = StringProperty(studio_data['url'], name='Url'),
            login = StringProperty(studio_data['login'], name='Login'),
            password = StringProperty(studio_data['password'], name='Password'),
        )

        #app.signals.projects_updated.connect(self.set_projects)
        app.signals.task_statuses_updated.connect(self.set_statuses)
        app.signals.task_types_updated.connect(self.set_shot_tasks)
        app.signals.task_types_updated.connect(self.set_asset_tasks)
        app.signals.entity_types_updated.connect(self.set_entity_types)
        app.signals.entity_types_updated.connect(self.set_asset_types)

        #app.signals.closed.connect(self.save)

        #print('Ignore Prefs', app.ignore_prefs)

        #if not app.ignore_prefs:
        #self.restore()

    def set_projects(self, projects):
        self.general['projects'].set_items([p.name for p in projects] )
 
    def set_statuses(self, statuses):
        items = [s.norm_name for s in statuses]
        self.general['statuses'].set_items(items)

    def set_shot_tasks(self, task_types):
        items = [t.name for t in task_types if t.for_shots and t.is_configured]
        self.general['shot_tasks'].set_items(items)

    def set_asset_tasks(self, task_types):
        items = [t.name for t in task_types if not t.for_shots and t.is_configured]
        self.general['asset_tasks'].set_items(items)

    def set_entity_types(self, entity_types):
        items = [e.name for e in entity_types if not e.for_assets] + ['Asset']
        self.general['entity_types'].set_items(items)

    def set_asset_types(self, entity_types):
        items = [e.name for e in entity_types if e.for_assets]
        self.general['asset_types'].set_items(items)

    def save(self):
        self.general.save('preferences/general')

        #print(self.general.to_dict())

        self.user.save('preferences/user')
        self.display.save('preferences/display')
        self.theme.save('preferences/theme')

    def restore(self):
        #print('restore', QSettings().value('preferences/general') )

        self.general.restore('preferences/general')
        self.user.restore('preferences/user')
        self.display.restore('preferences/display')
        self.theme.restore('preferences/theme')

        if not self.general.projects:
            self.general['projects'].set_items(CONFIG['projects'])

        if not self.general.environments:
            self.general['environments'].set_items(CONFIG['environments'])

        #print(576, self.general.projects, self.general['projects'].items)