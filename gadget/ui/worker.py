
from PySide2.QtCore import QThread, QObject, Signal, QTimer
import subprocess
##from gadget.constants import APP
import os
from time import time


class Job(QObject) :
    finished = Signal(object)

    def __init__(self, instance, *args, connect=None, **kargs) :
        super().__init__()

        self.instance = instance
        self.args = args
        self.kargs = kargs

        #print('instance', instance)
        #print('connect', connect)
        #print('args', args)
        #print('kargs', kargs)

        if connect :
            if not isinstance(connect, list) :
                connect = [connect]

            for c in connect :
                self.finished.connect(c)

    @classmethod
    def from_cmd(cls, cmd, connect=None, *args, **kargs) :
        kargs['env'] = os.environ.copy()
        return cls(subprocess.call, cmd, *args, connect=connect, **kargs)


class Worker(QThread) :
    def __init__(self, app) :
        super().__init__()

        self.app = app
        self.jobs = []
        #app.signals.closed.connect(self.close)
        self.is_running = True
        self.progress = 0

        #self.progress_timer = app.progress_timer
        #self.progress_timer.timeout.connect(lambda : self.app.signals.progress_updated.emit(self._progress))

        self.start()

    def add_job(self, instance, *args, connect=None, **kargs) :
        self.jobs.append( Job(instance, *args, connect=connect, **kargs) )
        self.update_progress()

    def add_cmd(self, cmd, *args, connect=None, **kargs) :
        self.jobs.append( Job.from_cmd(cmd, *args, connect=connect, **kargs) )
        self.update_progress()

    def stop(self) :
        self.is_running = False

    def update_progress(self) :
        if len(self.jobs) == 0 :
            progress = 0
        elif len(self.jobs) == 1 :
            progress = 85
        else :
            progress = 100 / (1+(len(self.jobs)/50) )**1.25

        if progress != self.progress :
            self.progress = progress
            self.app.signals.progress_updated.emit()

    def run(self) :
        while self.is_running:
            if not self.jobs:
                self.sleep(0.15)

                continue

            job = self.jobs[0]
            #print('\n>>>> Worker Thread Start', job.instance, *job.args, **job.kargs)

            try :
                result = job.instance(*job.args, **job.kargs)
                job.finished.emit(result)
                #if job.connect :

                #instance(*args, **kargs)
                # Job finished
            except Exception as e:
                print(e)

            del self.jobs[0]

            self.update_progress()





        print('\n>>>> Worker Thread Finished')
