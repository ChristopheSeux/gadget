
from canevas.ui.base_widgets import VWidget, HWidget, PushButton, TextLabel, \
Frame, LineEdit, VListWidget

from canevas.ui.widgets import ListEnum, StringLineEdit

import gadget
import os


class SoftsWindow(VWidget) :
    def __init__(self, parent=None):
        super().__init__(parent, title='Softs', margin=8, spacing=10, Background='dark')

        self.ui_setted = False

        gadget.app.signals.closed.connect(self.save)
        gadget.app.signals.project_updated.connect(self.set_ui)

    def set_ui(self):
        self.layout().clear()

        self.search_row = HWidget(self)

        self.state = ListEnum(self.search_row, items=['Installed', 'Available'], sizePolicy='Fixed')

        self.search_ledit = StringLineEdit(self.search_row, icon='search', clearBtn=True,
        connect=self.filter )

        Frame(self.search_row, tag='small')
        self.install_btn = PushButton(self.search_row, icon='import_32',
        connect=self.install_softs)

        self.uninstall_btn = PushButton(parent=self.search_row, icon='remove_32',
        connect=self.uninstall_softs)

        self.list_wgt = VListWidget(self, tag='AssetList', selectionMode='ExtendedSelection')
        
        self.set_softs()   
        self.ui_setted = True    

        self.restore() 

    def install_softs(self) :
        ## Add to the worker thead
        for soft_widget in self.list_wgt.selectedWidgets() :
            version = soft_widget.soft.versions.active
            gadget.app.disk_worker.add_job(version.install)

    def uninstall_softs(self) :
        ## Add to the worker thead
        for soft_widget in self.list_wgt.selectedWidgets() :
            version = soft_widget.soft.versions.active
            gadget.app.disk_worker.add_job(version.uninstall)

    def filter(self) :
        search = self.search_ledit.text().lower()
        for w in self.list_wgt.widgets() :
            soft_name = w.soft.name.lower()
            w.item.setHidden(search not in soft_name)

    def set_soft(self, soft):
        soft_widget = SoftWidget(soft)
        return self.list_wgt.addItem(text=soft.name, widget=soft_widget)

    def set_softs(self, softs=None) :
        if not softs :
            softs = gadget.context.project.softs

        for soft in softs :
            if soft.name == 'default' or soft.hide : continue
            self.set_soft(soft)

    def save(self):
        if not self.ui_setted :
            return

        data = {
            'selected': [i.text() for i in self.list_wgt.selectedItems()],
            'current': self.list_wgt.currentText(),
            'state': self.state.value()
        }
        QSettings().setValue('windows/'+self.windowTitle(), data )
    
    def restore(self):
        if gadget.app.ignore_prefs :
            return

        lw = self.list_wgt

        try:
            data = QSettings().value('windows/'+self.windowTitle())    
            self.state.setValue(data['state'])
            print('\nRestore', self.windowTitle(), data)
            for i in lw.items():
                print(i.text())
                i.setSelected(i.text() in data['selected'])
                if i.text() == data['current']:
                    lw.setCurrentItem(i)
                    
        except Exception as e:
            print('Failed to restore value for window', self.windowTitle())
            print(e)