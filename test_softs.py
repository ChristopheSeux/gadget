

from gadget.entities import Soft

blender_data = {
    'name' : 'Blender',
    'templates': {
        'path': '/path/to/blender',
        'archive': '/home/christophe/Téléchargements/blender-{version:[\d.]+}-{platform}{architecture}.{ext:.*}'
    }
}

blender = Soft(blender_data)

print(blender)

print(blender.templates)
